
jQuery(document).ready(function() {



	/**
	***
	***
	***
	*** Jquery Functions
	***
	***********************************/

	var win = jQuery(window),
			imagefeedelement,
			imagefeedimage,
	 		wait,
			countforwait,
			swait,
			incu,
			bodyele = $("body"),
			bodyurl = bodyele.data("url");
			amount = 4,
			jsfiles = Array();

	var resetVars = function() {
		incu = jQuery(".imagepage-container","#image_feed").last().data("page")+1;
		wait = true;
		countforwait = 0;
		swait = true;
		imagefeedelement = jQuery("#image_feed");
		imagefeedimage = jQuery("#start-logo > img");
		return false;
	};

	$("script[type='text/javascript']").each(function(){
		var ele = $(this);
		if(ele.data("reload")!=true)
		jsfiles.push(ele.attr("src"))
	})

	resetVars();




	/**
	***
	***
	***
	*** Jquery Functions
	***
	***********************************/

	jQuery.fn.reverse = function() {
		return this.pushStack(this.get().reverse(), arguments);
	};




	/**
	***
	***
	***
	*** CSS Styles
	***
	***********************************/

	var navheadcssxs = function() {
		bodyele.find(".nav-head").parent().unbind("click").bind("click",function(){
			var ulsel = jQuery(this).find("> ul");
			if(win.width() <= 768 && jQuery(this).find("> .nav-head").length > 0)
				jQuery(ulsel).slideToggle();
		});
	}

	var bindingscrolltop = function() {
			jQuery("#scrollToTheTop").bind('click', function() {
				$("html, body").stop().animate({ scrollTop: 0}, 700, 'swing')

				return false;
			})
	}

	// Cover
	// jQuery(".cover",bodyele).each(function() {
	// 	var child = jQuery(this).find("img.background").first();
	// 	if(child.length > 0)
	// 		jQuery(this).css("background-image", "url('"+child.attr("src")+"')")
	// });
	// jQuery(".cover-box").each(function() {
	// 	if(typeof (jQuery(this).attr("img-src")) !== 'undefined') {
	// 		jQuery(this).css("background-image", "url('"+jQuery(this).attr("img-src")+"')");
	// 		var posx = jQuery(this).attr("pos-x"),
	// 			posy = jQuery(this).attr("pos-y");
	// 		if(typeof posx === 'undefined' && typeof posy !== 'undefined') posx=50;
	// 		else if(typeof posx !== 'undefined' && typeof posy === 'undefined') posy=50;
	// 		jQuery(this).css("background-position", posx+"% "+posy+"%");
	// 	}
	// });





	/**
	***
	***
	***
	*** Own Functions
	***
	***********************************/

	// History rewrite
	var historyRewrite = function() {

		var oldurl = location.protocol+'//'+location.host+location.pathname+(location.search?location.search:"");
		var winheight = win.height(), winsctop = win.scrollTop();
		var urlchanged = false;
		var setURL = imagefeedelement.data("feed-url");
		var imagefeedpages = Array();

		imagefeedpages = jQuery("#image_feed>div").reverse();

		imagefeedpages.each(function() {
			var quasithis = jQuery(this);

			var elestart = quasithis.offset().top, eleend = quasithis.offset().top+quasithis.height();
			if (winsctop>winheight && elestart<winsctop && !urlchanged && jQuery(this).data("page") != 1) {
				setURL = imagefeedelement.data("feed-url")+
											"imagefeed/"+
											"p/"+quasithis.data("page")+
											"/type/"+imagefeedelement.data("feed-type");
				urlchanged=true;
				return false;
			}

		});
		history.replaceState({}, '', setURL);
		return false;

	}

	// Hide Logo if on top
	var hideLogoOnTop = function() {

		var ele = jQuery("#logoid");

		if(imagefeedimage.length>0) {

			var imgheight = imagefeedimage.offset().top+(imagefeedimage.height()*(1/4));

			if(imgheight < win.scrollTop() && ele.hasClass("hide")) {
				console.log("show Logo");
			  ele.removeClass("hide");
			}
			else if( imgheight >= win.scrollTop() && !(ele.hasClass("hide"))) {
				console.log("remove Logo");
				ele.addClass("hide");
			}

		}
		else {
			console.log("remove Logo");
			ele.removeClass("hide");
		}

		return false;
	}

	// show toTopscroller
	var showscrolltotopelement = function() {
		var show = win.height()*2.5<jQuery(document).scrollTop();
		var ele = $("#scrollToTheTop");
		if(show && !ele.hasClass("show") && ele.hasClass("hide")) {
			ele.addClass("show")
			ele.removeClass("hide")
		}
		else if(!show && ele.hasClass("show") && !ele.hasClass("hide")) {
			ele.removeClass("show")
			setTimeout(function() {
				ele.addClass("hide")
			},650);
		}
	};

	// Endless Scrolling
	var endlessscroll = function() {

		if(imagefeedelement.length>0) {

			// End of the document reached?
			if ((jQuery(document).height() - (win.height())*2.5) <= win.scrollTop()) {

				if((wait||countforwait>150 && swait)) {
					swait = false;

					jQuery('#loading').show();
					wait = false;

					console.log("imagefeed: load "+amount+" more ("+incu+")");
					var liste;
					liste = $("#image-feed").data("feed-type");
					var requesturl = imagefeedelement.data("feed-url")+'image_feed/ajax/'+incu+"&"+amount+"&"+liste

					jQuery.ajax({
						//url: imagefeedelement.data("feed-url")+'image_feed/ajax/s',
						url: requesturl,
						dataType: 'html',
						success: function(html) {
							if(html != 0) {
								var theresponse=jQuery(html);
								//console.log(requesturl);
								imagefeedelement.html(imagefeedelement.html()+theresponse[0].innerHTML);
								jQuery('#loading').hide();
								wait = true;
								incu+=1;
							} else {
								console.log("none found");
							}
							countforwait = 0;
							swait = true;
						}
					});

				}
				else {
					countforwait +=1;
				}
			}
		}
		return false;
	};






	/**
	***
	***
	***
	*** Ajax reload
	***
	***********************************/

	jQuery(function() {
	  String.prototype.decodeHTML = function() {
	    return jQuery("<div>", {html: "" + this}).html();
	  };

  	var $main = jQuery("#wrapperofall"),

			newJS = function(href) {
				jQuery.getJSON(href, { getJSAPIdata: "doit" }, function(data) {
					var arrayLength = data.length;
					for (var i = 0; i < arrayLength; i++) {
						var filename = data[i].replace(/^.*[\\\/]/, '').replace(/\.[^/.]+$/,"")
						helper = data[i].replace(bodyurl,'')
						if($.inArray(data[i],jsfiles)==-1) {
							jQuery.ajax({
							    async:false,
							    type:'GET',
							    url:data[i],
							    data:null,
							    success:function() {
										console.log("scipt geladen");
										jsfiles.push(data[i]);
									},
							    dataType:'script',
							    error: function(xhr, textStatus, errorThrown) {
							        // Look at the `textStatus` and/or `errorThrown` properties.
											console.log(textStatus,errorThrown)
							    }
							});
						}
						else {
							console.log("JS schon geladen");
						}
						bodyele.trigger("newpageload"+filename);
					}
				});
				return false;
			},

			newbodyClass = function(href) {
				jQuery.getJSON(href, { getBODYAPIdata: "doit" }, function(data) {
					bodyele.attr("class",data);
				});
				return false;
			},

      init = function(href) {
        console.log("load: "+href);
				resetVars();
				if(href != "/start") {
				  newJS(href);
					newbodyClass(href);
				}
				bodyele.trigger("newpageload-every");
      },

      ajaxLoad = function(html, href) {
        document.title = html
          .match(/<title>(.*?)<\/title>/)[1]
          .trim()
          .decodeHTML();

        init(href);
      },

		  loadPage = function(href) {
		    $main.load(href + " #wrapperofall>*", function(html){ ajaxLoad(html, href) });
		  };

		init("/start");

		win.on("popstate", function() {
	    loadPage(location.href);
	  });

	  jQuery(document).on("click", "a, area", function() {
    	var href = jQuery(this).attr("href");

			if (!(jQuery(this).hasClass("up")) && !(jQuery(this).hasClass("down"))) {
		    if (href.indexOf(document.domain) > -1
		      || href.indexOf(':') === -1)
		    {
					if($(this).attr("rel") != "ajax")
		      	history.pushState({}, '', href);
		      loadPage(href);
		      return false;
		    }
			}
	  });
	});

	var updownlinks = function() {
		$("a.up,a.down").on("click",function(event){
			event.preventDefault();
	    var row = $(this).parents("tr:first"), tmp = 0, change;
	    if ($(this).is(".up")) {
					change = row.prev();
	        row.insertBefore(change);

	    } else {
					change = row.next();
	        row.insertAfter(change);
	    }
			var dataname = "db-position";
			tmp = row.data(dataname);
			row.addClass("changed");
			change.addClass("changed");
			row.data(dataname,change.data(dataname));
			change.data(dataname,tmp);

			var tbody = row.parents("tbody:first"),
					href = tbody.attr("action"), index,len,rtrn = Array(),
					alltr = jQuery("> tr.changed",tbody);

			for (index=0, len = alltr.length; index<len; ++index) {
				var tmpar = jQuery(alltr[index]);
				rtrn.push({
					key: tmpar.data(dataname),
					id: tmpar.data("id")
				});
			}

			jQuery.post(href, { imagerow: JSON.stringify(rtrn) }, function(data) {
				if(!data) {
					console.log("Bearbeitung der Reihenfolge fehlgeschlagen")
				}
			});
		});
	}






	/**
	***
	***
	***
	*** Bind Functions
	***
	***********************************/

	hideLogoOnTop();
	historyRewrite();
	showscrolltotopelement()
	bodyele.trigger("newpageload");
	bodyele.trigger("newpageload-every");

	win.bind('scroll touchmove', function() {
		hideLogoOnTop();
		historyRewrite();
		endlessscroll();
		showscrolltotopelement();
	});

	win.bind('resize', function() {
		win = $(window);
	});

	bodyele.unbind("newpageload-every").bind("newpageload-every", function() {
		navheadcssxs();
		bindingscrolltop();
		updownlinks();
	});

});
