
$(document).ready(function() {
  Dropzone.autoDiscover = false;
});


// Dropzone.autoDiscover = false;
  var dodropzone = function() {
    var drpzcon = $("form#imageUploadContainer");
    var drp_res = drpzcon.dropzone({
                url: drpzcon.attr("action"),
                dictDefaultMessage: "Drag your images",
                init: function() {
                      thisDropzone = this;
                      this.on("success", function(file, responseText) {
                           $("#righttop-nextstep").addClass("show");
                               console.log(responseText);
                      });
                  },
                paramName: "file",
                maxFilesize: 30, // MB
                cceptedFiles: "image/*",
                accept: function(file, done, html) {
                    if (file.type != "image/jpeg") {
                        done("Error! Files of this type are not accepted");
                    }
                    else if (file.size < 1.3*1024*1024) {
                        done("Error! This File is smaller than 1.3MB");
                    }
                    else { done(); }
                }
              });
  }
  $("body").bind("newpageload newpageloaddrp",function() {
    dodropzone();
    var form = $("form#imageUploadContainer");

    if($("> .fallback",form).length>0 && form.length>0) {
      console.log("try again")
      setTimeout(dodropzone(),200);
    }
  });
