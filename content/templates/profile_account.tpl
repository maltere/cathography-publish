{include file="profile_std.tpl"}
<p>Hier kannst du deine Nutzerdaten bearbeiten.</p>
<h5>Dein Profilbild</h5>
<a href="http://gravatar.com"><img src="{$loggedin_user->getGravatarUrl(100)}" alt="Gravatar Bild"></a>
<p>Wir verwenden für die Verwaltung der Profilbilder Gravatar.
  Gravatar ist eine Seite mit der Profilbilder global
  auf mehreren Seiten verwaltet werden können.
  Wir bekommen das Bild über deine Emailadresse.
  falls du dort mit deiner Email einen Account hast.
  Hast du keinen Account bei Gravatar benutzen wir ein Standard Bild von uns.</p>
