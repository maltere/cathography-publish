<h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
<p>Hier sind alle registrierten Benutzer aufgelistet.<p>
{if $addAccount[0]}
<p class="sys_message {$addAccount[2]|lower}">
  {if $addAccount[2] == "notice"}
  Der Account '{$addAccount[1]}' wurde erfolgreich erstellt.
  {elseif $addAccount[2] == "error"}
  Ein Fehler ist beim erstellen des Account '{$addAccount[1]}' aufgetreten.
  {/if}
</p>
{/if}
<div class="container-fluid">
  <form method="post" action="{$smarty.const.URL_PATH}admin/account/add/now/" accept-charset="UTF-8">
    <div class="row row-form row-border-no-padding row-margin-top">
      <div class="col col-6">
        <input type="text" name="username" placeholder="USER" class="fullscreen">
      </div>
      <div class="col col-6">
        <input type="text" name="email" placeholder="EMAIL" class="fullscreen">
      </div>
    </div>
    <div class="row row-form row-border-no-padding row-margin-top">
      <div class="col col-6">
        <input type="text" name="firstname" placeholder="VORNAME" class="fullscreen">
      </div>
      <div class="col col-6">
        <input type="text" name="lastname" placeholder="NACHNAME" class="fullscreen">
      </div>
    </div>
    <div class="row row-form row-border-no-padding row-margin-top">
      <div class="col col-6">
        <input type="password" name="kennwort" placeholder="PASSWORT" class="fullscreen">
      </div>
      <div class="col col-6">
        <input type="password" name="passwordw" placeholder="PASSWORT WIEDERHOLEN" class="fullscreen">
      </div>
    </div>
    <div class="row row-form row-border-no-padding row-margin-top">
      <div class="col col-12">
        <input type="submit" name="sub" value="Login" class="fullscreen">
      </div>
    </div>
  </form>
</div>
