<div class="xs-only row row-border-no-padding padding-top-small padding-bottom-small background-bright">
  <div class="col col-12">
    <img src="{$imageofview->getURL("large")}">
  </div>
</div>
<div id="leftbottom-info">
  <a href="{$imageofview->getUser()->getProfilUrl()}" class="full"></a>
  <div class="row">
    <div class="col col-8">
      <h6>{$imageofview->getUser()->getName()}</h6>
    </div>
    <div class="col col-4 profilepic">
      <img src="{$imageofview->getUser()->getGravatarUrl()}">
    </div>
  </div>
</div>
