<h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
<p>Deine Bilder die du hochgeladen hast (Freigeschaltet und nicht)!<p>
<table class="listed">
  <tr>
    <th class="text-align-center">#</th>
    <th></th>
    <th>Uploaddate</th>
    <th></th>
    <th></th>
  </tr>
{foreach key=key item=item from=$yourimagelist}
  <tr>
    <td class="text-align-center">{counter}</td>
    <td><img src="{$item->getURL("small")}"></td>
    <td>{$item->getUploadDate("d.m.Y H:i \U\h\\r")}</td>
    <td class="link"><a href="{$item->getURL()}">zur Ansicht</a></td>
    <td class="link"><a href="{$item->getURL("delete")}" rel="ajax">Löschen</a></td>
  </tr>
{/foreach}
</table>
