<h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>

<div class="container-fluid">
  <form method="post" action="{$smarty.const.URL_PATH}admin/frontend/edit/">
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-4 empty"><h4>Projektname</h4></div>
        <div class="col col-8">
            <input type="text" name="edit_page_title" value="{$frontend_menu["page_title"]}" class="fullscreen">
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top">
      <div class="col col-12">
        <h4>Text auf der Startseite</h4>
        <textarea class="fullscreen" name="welcome_message">{$frontend_menu["startpagetext"]}</textarea>
      </div>
    </div>
    <div class="row row-border-no-padding row-margin-top">
        <div class="col col-8 empty"></div>
        <div class="col col-4">
            <input type="submit" name="submit" value="Absenden" class="fullscreen">
        </div>
    </div>
  </form>
</div>
