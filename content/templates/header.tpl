<!doctype html>

<html lang="{$header.lang}">
<head>
  <meta charset="utf-8">

  <title>{if not empty($page_subtitle)}{$page_subtitle} - {/if}{$header.title}</title>
  <meta name="description" content="{$header.description}">
  <meta name="author" content="{$header.author}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <meta property="og:url" content="{$smarty.const.URL_PATH}" />

{foreach from=$ExternalCSS item=$cssvar}
  <link rel="stylesheet" href="{$cssvar}">
{/foreach}
{foreach from=$CSS item=$cssvar}
  <link rel="stylesheet" href="/{$system.url|rtrim:'/'|trim:'/'}/stylesheets/{$cssvar}">
{/foreach}

  {foreach from=$addToHeader item=$file}
{include file=$file}
  {/foreach}

  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body id="body" class="{$body_classes}" data-url="{$smarty.const.URL_PATH}">
  <div id="wrapperofall">
