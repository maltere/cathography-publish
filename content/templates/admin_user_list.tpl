<h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
<p>Hier sind alle registrierten Benutzer aufgelistet.<p>
<table class="listed">
  <tr>
    <th class="text-align-center">#</th>
    <th>Name</th>
    <th>Username</th>
    <th>Email</th>
    <th></th>
  </tr>
{foreach key=key item=item from=$userrow}
  <tr {if $item==$loggedin_user}class="selected"{/if}>
    <td class="text-align-center">{counter}</td>
    <td>{$item->getName()}</td>
    <td>{$item->getUsername()}</td>
    <td>{$item->getEmail()}</td>
    <td class="link"><a href="{$smarty.const.URL_PATH|rtrim:'/'}/admin/account/list/id/{$item->getID()}/delete/{$item->getHash("delete")}" rel="ajax">Löschen</a></td>
  </tr>
{/foreach}
</table>
