<h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
<p>Hier sind alle Bilder die gefeatured werden sollen oder bereits sind.<p>
{if $changerows}
<form action="{$adminpage->getURL(1)}?edit=1" method="post">
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-7 empty"><h5>Anzahl Bilder welche veröffentlicht werden:</h5></div>
        <div class="col col-4">
            <input type="text" name="edit_number_of_pics" value="{$releaseForm.number_of_pic}" class="fullscreen small">
        </div>
        <div class="col col-1">
          Bilder
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-4 empty"><h5>Veröffentlichung alle:</h5></div>
        <div class="col col-7">
            <input type="text" name="edit_release_intervall" value="{$releaseForm.release_intervall}" class="fullscreen small">
        </div>
        <div class="col col-1">
          Tage
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-4 empty"><h5>Veröffentlichung um:</h5></div>
        <div class="col col-7">
            <input type="text" name="edit_release_time" value="{$releaseForm.release_time}" class="fullscreen small" placeholder="HH:mm">
        </div>
        <div class="col col-1">
          Uhr
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-4 empty"><h5>Veröffentlichung start:</h5></div>
        <div class="col col-7">
            <input type="text" name="edit_release_date" value="{$releaseForm.release_start_date}" class="fullscreen small" placeholder="YYYY-MM-DD">
        </div>
        <div class="col col-1">

        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top">
        <div class="col col-8 empty"></div>
        <div class="col col-4">
            <input type="submit" name="submit" value="Absenden" class="fullscreen small">
        </div>
    </div>
</form>
{/if}
<h4 class="row-margin-top">Unveröffentlicht in Featured</h4>
<p>Diese Bilder befinden sich aktuell in der Warteschleife.</p>
<table class="listed row-margin-top">
  <thead>
    <tr>
      <th class="text-align-center">#</th>
      <th></th>
      <th>Uploaddate</th>
      <th colspan="3"></th>
    </tr>
  </thead>
  <tbody action="{$smarty.const.URL_PATH|rtrim:'/'}/admin/image/featuring/">
{foreach key=key item=item from=$yourimagelist}
    <tr{if !empty($item->getTmpCategory())} data-db-position="{$item->getTmpCategory()->getPosition()}"{/if} data-id="{$item->getID()}" id="image{$item->getID()}" class="image{$item->getID()}">
      <td class="text-align-center">{counter}</td>
      <td><img src="{$item->getURL("small")}"></td>
      <td>{$item->getUploadDate("d.m.Y H:i \U\h\\r")}</td>
      {if empty($item->getCategory("featured"))}
      <td class="link"><a href="{$adminpage->getURL(1)}addcategory/featured/id/{$item->getID()}">add to featuring</a></td>
      {else}
      <td class="link"><a href="{$adminpage->getURL(1)}removecategory/featured/id/{$item->getID()}">remove from featuring</a></td>
      {/if}
      <td class="link"><a href="{$item->getURL()}">zur Ansicht</a></td>
      {if $changerows}
      <td class="link"><a href="" class="up">Up</a> <a href="" class="down">Down</a></td>
      {else}
      <td class="link"><a href="{$item->getURL("delete")}" rel="ajax">Löschen</a></td>
      {/if}
    </tr>
{/foreach}
  </tbody>
</table>
