{include file="profile_std.tpl"}
{if $changePW[0]}
<p class="sys_message {$changePW[1]|lower}">
  {if $changePW[1] == "notice"}
  Das Kennwort wurde geändert
  {elseif $changePW[1] == "error"}
  Es ist ein Fehler aufgetreten
  {/if}
</p>
{/if}
<form action="{$adminpage->getURL(1)}?edit=1" method="post">
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-5 empty"><h5>Neues Kennwort:</h5></div>
        <div class="col col-7">
            <input type="password" name="npw" value="{$releaseForm.release_intervall}" class="fullscreen small">
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-5 empty"><h5>Neues Kennwort (wdh):</h5></div>
        <div class="col col-7">
            <input type="password" name="npwwdh" value="{$releaseForm.release_time}" class="fullscreen small" placeholder="">
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top row-align-middle">
        <div class="col col-5 empty"><h5>Altes Kennwort:</h5></div>
        <div class="col col-7">
            <input type="password" name="oldpw" value="{$releaseForm.release_start_date}" class="fullscreen small" placeholder="">
        </div>
    </div>
    <div class="row row-border-no-padding row-margin-top">
        <div class="col col-8 empty"></div>
        <div class="col col-4">
            <input type="submit" name="submit" value="Absenden" class="fullscreen small">
        </div>
    </div>
</form>
