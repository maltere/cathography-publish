
<div class="login-top">
{if $loggedin}
  <a href="{$smarty.const.URL_PATH}profile" class="small-button admin">Profil</a>
  {if $loggedin_user->isAdmin()}
  <a href="{$smarty.const.URL_PATH}admin" class="small-button admin">Admin</a>
  {/if}
  <a href="{$smarty.const.URL_PATH}logout" class="small-button logout">Logout</a>
{else}
  <a href="{$smarty.const.URL_PATH}login" class="small-button login">Login</a>
  <a href="{$smarty.const.URL_PATH}register" class="small-button register">Register</a>
{/if}
</div>
