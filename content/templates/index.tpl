<div class="row row-margin-top">
  <div class="col col-3 text-align-left" id="start-logo">
    <img src="{$smarty.const.URL_PATH}img/logo.svg" class="img-half margin-top">
    <div class="container-fluid margin-top">
        <h6 class="caps">Powered By</h6>
        <img src="{$smarty.const.URL_PATH}img/kpressebrunddreizeiler.png">
    </div>
  </div>
  <div class="col col-7 text-align-center align-center" id="welcome">
    <h1 class="xs-hide">{$page_title}</h1>
    {$welcome_message|ptags}
  </div>
</div>
