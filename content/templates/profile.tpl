<div class="container-fluid" id="settingscontainer">

    <nav class="nav menu-adminmenu">
      <ul>
        {foreach key=keyy item=itemm from=$adminmenu[0]}
        <li><a href="{$smarty.const.URL_PATH|rtrim:'/'}/{$itemm.path|trim:'/'}/i" class="{$itemm.css}">{$itemm.title}</a></li>
        {/foreach}
      </ul>
    </nav>
    <div class="row row-margin-top">
      <div class="col col-3">
          <nav class="nav-simple nav-leftsub menu-adminmenu">
            <ul>
              <li><a href="{$smarty.const.URL_PATH|rtrim:'/'}/profile/{$adminpage->slug}/i">Übersicht</a></li>
              {foreach key=adminsubkeyy item=adminsubmen from=$adminmenu[1]}
              <li><a href="{$smarty.const.URL_PATH|rtrim:'/'}/profile/{$adminpage->slug}/{$adminsubmen.slug}/" class="{$adminsubmen.css}">{$adminsubmen.title}</a></li>
              {/foreach}
            </ul>
          </nav>
      </div>
      <div class="col col-9 admin-container">
