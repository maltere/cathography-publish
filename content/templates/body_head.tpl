{include file='top_admin.tpl'}
  <div id="main" class="container-fluid">
    {if $mainstyle!=null}
    <style type="text/css">
    #main {
      {$mainstyle}
    }
    </style>
    {/if}
    <div class="container">
    <div class="row" id="maintoprow">
      <a href="{$smarty.const.URL_PATH}" id="logoid" class="{if $logoontiphidden}hide{/if}{if isset($isBright) and $isbright} dark{elseif isset($isBright) and !$isbright} bright{/if}">
        <img src="{$smarty.const.URL_PATH}img/{if isset($isBright) and $isbright}logosw.svg{elseif isset($isBright) and !$isbright}logosw.svg{else}logo.svg{/if}">
        <div class="hover"><img src="{$smarty.const.URL_PATH}img/logosw.svg"></div>
      </a>
    </div>
    <div class="row row-align-bottom" id="header">
      <div class="col col-10 col-no-padding">
{include file='menu.tpl'}
      </div>
    </div>
