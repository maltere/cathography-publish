{assign var="image_feed" value=true scope=global nocache}
<div class="container-fluid margin-top" id="image_feed" data-feed-url="{$smarty.const.URL_FULL_PATH}" data-feed-type="{$feedtype}">
{foreach from=$ImageFeed key=ImageFeedPage item=ImageFeedPageItem}
{counter print=false start=0 name="imgcounter"}
<div data-page="{$ImageFeedPage}" class="imagepage-container">
{foreach from=$ImageFeedPageItem item=imagefeeditem}
  <div class="row row-margin-top " data-image="{counter print=true name=imgcounter}">
    <div class="col col-12">
      <div class="container-fluid relative imagefeeditem imagecontainer{$imagefeeditem->getID(true)}">
        <style type="text/css">
          .imagecontainer{$imagefeeditem->getID(true)} {
            background-color: {$imagefeeditem->averageColor("hex",1.3,40)};
            position:relative;
          }
          .imagecontainer{$imagefeeditem->getID(true)}:before {
            content:"";
            display: block;
            width:100%;
            padding-top:{$imagefeeditem->getResolution()}%;
          }
          .imagecontainer{$imagefeeditem->getID(true)} > img {
            position: absolute;
            top: 0;
            display: block;
            bottom:0;
            left:0;
            right:0;
            z-index: 199;
          }
        </style>
        <a href="{$imagefeeditem->getURL()}"></a>
        <img src="{$imagefeeditem->getURL("normal")}" alt="Bild: {$imagefeeditem->getName()}">
        <div class="absolute">

        </div>
      </div>
    </div>
  </div>
{if $showSponsored}
  <div class="row row-margin-top">
    <div class="col col-12">
      <h6 class="caps">Sponsored By</h6>
      <div class="row row-border-no-padding">
        <div class="col-1">
          <a href="//maltereddig.de"><img src="{$smarty.const.URL_PATH|rtrim:"/"}/img/maltere.svg"></a>
        </div>
      </div>
    </div>
  </div>
{assign var=showSponsored value=false}
{/if}
{/foreach}
</div>
{/foreach}
</div>
<div id="loading"></div>
