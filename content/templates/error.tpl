{foreach from=$errors item=$err}
  <p class="sys_message {$err.type|lower}">{$err.type} <span class="code">{$err.code}</span>{if $smarty.const.DEBUG} in <strong>{$err.file}:{$err.line}</strong><br>{else}: {/if}{$err.message}</p>
{/foreach}
