
  <nav class="nav menu-{Menu_Container::get_menu_id("mainmenu")} menu-mainmenu" id="main-menu">
    <div class="nav-head">
        <h3 class="xs-only">{$page_title}</h3>
        <div class="nav-button"><i class="fa fa-bars"></i></div>
    </div>
    <ul>
      {foreach key=keyy item=itemm from=$menu_items.mainmenu}
      <li><a href="{$smarty.const.URL_PATH|rtrim:'/'}/{$itemm->getPath()|ltrim:'/'}" class="{$itemm->getCSS()}">{$itemm->getTitle()}</a></li>
      {/foreach}
    </ul>
  </nav>
