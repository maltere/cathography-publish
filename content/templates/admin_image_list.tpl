<h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
<p>Alle hochgeladenen und freigegebenen Bidler.<p>
<table class="listed row-margin-top">
  <thead>
    <tr>
      <th class="text-align-center">#</th>
      <th></th>
      <th>Uploaddate</th>
      <th colspan="3"></th>
    </tr>
  </thead>
  <tbody action="{$smarty.const.URL_PATH|rtrim:'/'}/admin/image/featuring/">
{foreach key=key item=item from=$yourimagelist}
    <tr{if !empty($item->getTmpCategory())} data-db-position="{$item->getTmpCategory()->getPosition()}"{/if} data-id="{$item->getID()}" id="image{$item->getID()}" class="image{$item->getID()}">
      <td class="text-align-center">{counter}</td>
      <td><img src="{$item->getURL("small")}"></td>
      <td>{$item->getUploadDate("d.m.Y H:i \U\h\\r")}</td>
      {if empty($item->getCategory("featured"))}
      <td class="link"><a href="{$adminpage->getURL(1)}addcategory/featured/id/{$item->getID()}">add to featuring</a></td>
      {else}
      <td class="link"><a href="{$adminpage->getURL(1)}removecategory/featured/id/{$item->getID()}">remove from featuring</a></td>
      {/if}
      <td class="link"><a href="{$item->getURL()}">zur Ansicht</a></td>
      {if $changerows}
      <td class="link"><a href="" class="up">Up</a> <a href="" class="down">Down</a></td>
      {else}
      <td class="link"><a href="{$item->getURL("delete")}" rel="ajax">Löschen</a></td>
      {/if}
    </tr>
{/foreach}
  </tbody>
</table>
