<div class="container-fluid relative display-none">
  <h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
  <p>Lade hier deine Bilder hoch. Im 2. Schritt solltest du deine Bilder noch einmal überprüfen ob alle Angaben soweit stimmen bevor du die Bilder dann entgültig unter der <strong>CC0</strong> veröffentlichst.<p>
  <form action="{$smarty.const.URL_PATH|rtrim:'/'}/profile/image/upload" class="dz-clickable" id="imageUploadContainer">
    <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
    <div class="fallback">
      <input name="file" type="file" multiple />
    </div>
  </form>
  <a href="{$smarty.const.URL_PATH|rtrim:'/'}/profile/image/review/cat/unpublished" id="righttop-nextstep" class="button">Nächster Schritt</a>
</div>
