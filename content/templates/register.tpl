<div class="container-fluid text-align-center">

    <h1>Neuen Account anlegen</h1>
    <div class="row">
      <div class="col col-8 align-center text-align-left">
        <form method="post" action="{$smarty.const.URL_PATH}register/user" accept-charset="UTF-8">
          <div class="row row-form row-border-no-padding row-margin-top">
            <div class="col col-6">
              <input type="text" name="username" placeholder="USER" class="fullscreen">
            </div>
            <div class="col col-6">
              <input type="text" name="email" placeholder="EMAIL" class="fullscreen">
            </div>
          </div>
          <div class="row row-form row-border-no-padding row-margin-top">
            <div class="col col-6">
              <input type="text" name="firstname" placeholder="VORNAME" class="fullscreen">
            </div>
            <div class="col col-6">
              <input type="text" name="lastname" placeholder="NACHNAME" class="fullscreen">
            </div>
          </div>
          <div class="row row-form row-border-no-padding row-margin-top">
            <div class="col col-6">
              <input type="password" name="kennwort" placeholder="PASSWORT" class="fullscreen">
            </div>
            <div class="col col-6">
              <input type="password" name="passwordw" placeholder="PASSWORT WIEDERHOLEN" class="fullscreen">
            </div>
          </div>
          <div class="row row-form row-border-no-padding row-margin-top">
            <div class="col col-12">
              <input type="checkbox" name="agb" id="agb" value="1">
              <label class="fullscreen" for="agb">
                Ich akzeptiere die AGB's.
              </label>
            </div>
          </div>
          <div class="row row-form row-border-no-padding row-margin-top">
            <div class="col col-12">
              <input type="submit" name="sub" value="Login" class="fullscreen">
            </div>
          </div>
        </form>
      </div>
    </div>
</div>
