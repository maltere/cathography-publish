
  <h1>{$adminpage->title} <small>{$adminpage->subtitle}</small></h1>
  {if !$noImagetoReview}
  <p>Überprüfe jedes Bild ob die Informationen da zu passen.</p>
  <form action="{$reviewlink}{$ImageReview->getID()}" method="post">
    <div class="row row-border-no-padding row-margin-top">
      <div class="col col-4">
        <a href="#" class="fullscreen button">Zum letzten Bild</a>
      </div>
      <div class="col col-4">
        <a href="#" class="fullscreen button">Zum nächsten Bild</a>
      </div>
      <div class="col col-4">
        <input type="submit" name="release" class="fullscreen button" value="Freigeben und weiter">
      </div>
    </div>
    <div class="row row-border-no-padding row-margin-top">
      {foreach from=$reviewError item=erroruno}
      <p class="sys_message error">Das Bild wurde nicht freigegeben weil:<br><strong>{$erroruno.msg}</strong></p>
      {/foreach}
    </div>
    <div class="row row-border-no-padding row-margin-top">
      <div class="col col-4">
        <img src="{$ImageReview->getURL("normal")}" alt="Bild: {$ImageReview->getName()}">
        <p><a href="{$smarty.const.URL_PATH}profile/image/review/id/{$ImageReview->getID()}/delete/{$ImageReview->getHash("delete")}" class="button fullscreen red">Bild löschen</a></p>
      </div>
      <div class="col col-8">
        <p>
          <input type="checkbox" name="cco" id="cco" value="1" checked>
          <label class="fullscreen" for="cco">
            Ich bestätige das ich das Bild unter der CCO Lizenz freigeben möchte.
          </label>
        </p>
        <p>
          <h6>Schlagwörter <small>(mit kommas trennen)</small></h6>
          <textarea class="fullscreen" name="tags" placeholder="Schlagwörter"></textarea>
        </p>
        <p>
          <h6>Location <small>(Freitext)</small></h6>
          <input type="text" class="fullscreen" placeholder="Nicht gesetzt" value="">
        </p>
      </div>
    </div>
    <div class="row row-border-no-padding row-margin-top">
      <div class="col col-4">
        <a href="#" class="fullscreen button">Zum letzten Bild</a>
      </div>
      <div class="col col-4">
        <a href="#" class="fullscreen button">Zum nächsten Bild</a>
      </div>
      <div class="col col-4">
        <input type="submit" name="release" class="fullscreen button" value="Freigeben und weiter">
      </div>
    </div>
  </form>
{else}
<p>Es sind keine Bilder vorhanden die gereviewt werden müssen.</p>
{/if}
