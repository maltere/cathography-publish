<div class="row row-margin-top">
  <div class="col col-2 text-align-left" id="start-logo">
    <img src="{$user->getGravatarUrl()}" class="margin-top">
  </div>
  <div class="col col-8 text-align-center align-center" id="welcome">
    <h1 class="xs-hide">{$user->getUsername()}<br><small>{$user->getName()}</small></h1>

  </div>
</div>
