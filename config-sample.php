<?php

/* Set DIR_Path */
define("SYSTEM_PATH", "./");

/* Exception anzeigen: True or False */
define("EXCEPTION_SHOW", TRUE);

/* Set Error_Log File */
define("ERROR_LOG_FILE", SYSTEM_PATH."log/sys-error.log");

/* Datenbank System */
define("DB_SYSTEM", "MYSQL");

/* Datenbank Host */
define("DB_HOST","localhost");

/* Datenbank User */
define("DB_USER","root");

/* Datenbank Password */
define("DB_PASSWORD","root");

/* Datenbank Name */
define("DB_NAME","vsprojekt");

/* Datenbank Tabellen Prefix */
define("DB_TB_PRE","vs_");

/* Fehlerlevel */
error_reporting(E_ALL & ~E_NOTICE);
