<?php
/*******************

  VSoftware 0.03.0b

  Das ist eine Software die als Grundstruktur für einige Seiten gelten soll.
  Ziel ist es eine Modulare Grundstruktur als Basis für die Seite zu verwenden.
  Das System wird von Malte Reddig entwickelt.

  Wichtige Informationseinstellungen in config.php vornehmen.

*******************/

// System starten.
global $stime;
$stime = microtime();
require_once "config.php";
require_once SYSTEM_PATH."system/exception.php";
require_once SYSTEM_PATH."system/init.php";

try {

  /* System wird gestartet.
  ** True:  System erfolgreich gestartet.
  ** False: Fehler beim Systemstart */
  if(  System::init()  )  {

    /* System Funktionen starten */

    Callback::init("System_init_first");
    Callback::init("System_init_second");
    Callback::init("System_init_third");

  }
  else {
    throw new SysExc("System konnte nicht gestartet werden.",0);
  }
}
catch(SysExc $e) {

  $e->getErrorMessage(EXCEPTION_SHOW);

}

//var_dump(microtime()-$stime);

?>
