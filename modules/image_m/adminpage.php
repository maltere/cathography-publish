<?php

class Adminpage extends apgexmpl {

  protected $module = "image_m";

  public $title = "Bilder";
  public $slug = "image";

  public function __construct() {

    Smarty_i::setTempInRow("admin.tpl","Smarty_display_admin_head");
    Smarty_i::setTempInRow("admin_footer.tpl","Smarty_display_admin_footer");

    $this->pre_function();

    $page["list"] = array("pList","Bilderliste");
    $page["featuring"] = array("pFeatured","Featuredliste");

    $this->switchPage($page);
    $this->createSubMenu($page);

    Smarty_i::setTempInRow($this->subtemplate,"Smarty_display_admin_main");
  }

  protected function pre_function() {

    $gv = new GV(2);

    if(!empty(GV::get("delete")) && !empty(GV::get("id"))) {
      Account::remove(GV::get("id"), GV::get("delete"));
    }
    if(strlen($gv->getV("addcategory"))>0) {
      $cate = Database::em()->findOneBy("Image_Category",array("slug"=>$gv->getV("addcategory"),"group"=>System::$group));
      $image = Database::em()->findOneBy("Image",array("id"=>$gv->getV("id")));
      if($image != NULL) {
        $cate->addImage($image);
        Database::em()->flush();
      }

    }
    if(strlen($gv->getV("removecategory"))>0) {
      $cate = Database::em()->findOneBy("Image_Category",array("slug"=>$gv->getV("removecategory"),"group"=>System::$group));
      if($cate != NULL) {
        $conn = Database::em()->findBy("Image_Category_Link",array("category"=>$cate,"image"=>$gv->getV("id")));
        if($conn != NULL) {
          foreach($conn as $conne) {
            Database::em()->remove($conne);
          }
          Database::em()->flush();
        }
      }

    }
    if(strlen(GV::get("imagerow"))>0) {
      $cate = Database::em()->findOneBy("Image_Category",array("slug"=>$gv->getV("addcategory"),"group"=>System::$group));
      $featured = Database::em()->findOneBy("entities\Image_Category",array("slug"=>"featured"));
      echo json_encode($this->editPositon(json_decode(GV::get("imagerow"),true),$featured));
      exit();
    }
  }

  protected function pList() {
    $this->subtitle = "Cathography Bilderliste";
    $this->subtemplate = "admin_image_list.tpl";
    $this->subslug = "list";

    $qb = Database::em()->qb();
    $qb->select("icl")->from("entities\Image_Category_Link","icl")->join("icl.image","i")->join("icl.category","ic")->where("i.group = :group")->andWhere("ic.slug = :uploaded");
    $qb->orderBy("i.id","DESC");
    $qb->setParameter("group",System::$group)->setParameter("uploaded","uploaded");
    $icl = $qb->getQuery()->getResult();
    $images = toImages($icl);

    Smarty_i::setVar("yourimagelist",$images);
  }

  protected function pFeatured() {
    $this->subtitle = "Cathography Bilderliste";
    $this->subtemplate = "admin_image_list_fea.tpl";
    $this->subslug = "featuring";

    if((int)GV::get("edit","GET")===1) {
      Meta_daten::setVar("release_intervall",GV::get("edit_release_intervall","POST"),NULL,"image_m");
      Meta_daten::setVar("release_time",GV::get("edit_release_time","POST"),NULL,"image_m");
      Meta_daten::setVar("release_start_date",GV::get("edit_release_date","POST"),NULL,"image_m");
      Meta_daten::setVar("number_of_pic",GV::get("edit_number_of_pics","POST"),NULL,"image_m");
    }
    $releaseForm["release_intervall"] = Meta_daten::getVar("release_intervall","image_m","val");
    $releaseForm["release_time"] = Meta_daten::getVar("release_time","image_m","val");
    $releaseForm["release_start_date"] = Meta_daten::getVar("release_start_date","image_m","val");
    $releaseForm["number_of_pic"] = Meta_daten::getVar("number_of_pic","image_m","val");

    $qb = Database::em()->qb();
    $qb->select("icl")->from("entities\Image_Category_Link","icl")->join("icl.image","i")->join("icl.category","ic")->where("i.group = :group")->andWhere("ic.slug = :featured");
    $qb->orderBy("icl.position","ASC");
    $qb->setParameter("group",System::$group)->setParameter("featured","featured");
    $icl = $qb->getQuery()->getResult();
    $images = toImages($icl);

    Smarty_i::setVar("yourimagelist",$images);
    Smarty_i::setVar("changerows",true);
    Smarty_i::setVar("releaseForm",$releaseForm);

  }

  protected function pIndex() {

  }

  protected function editPositon($rows,entities\Image_Category $cat) {
    if(is_array($rows)) {
      $image_link = array();
      foreach($rows as $row) {
        $image_link_t = Database::em()->findOneBy("entities\Image_Category_Link",array("image"=>(int)$row["id"],"category"=>$cat));
        if($image_link_t != NULL) {
          $image_link_t->setPosition((int)$row["key"]);
          $image_link[] = &$image_link_t;
        }
      }
      Database::em()->flush();
      return true;
    }
    else {
      return false;
    }
  }

}

function toImages($image_array) {
  $return = array();
  foreach($image_array as $link) {

    $tmp = $link->getImage();
    $tmp->setTmpCategory($link);
    $return[] = $tmp;
  }
  return $return;
}
