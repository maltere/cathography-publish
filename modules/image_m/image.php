<?php

class Image {

  public $somenumber;
  public $res;
  public $abs_url;
  public $rel_url;
  public $abs_uri;
  public $rel_uri;
  public $sizes_url;
  public $large_url;
  public $normal_url;
  public $small_url;
  public $medium_url;
  public $fullsz_url;
  public $modified;
  public $db_uri;
  public static $upload_path = "upload/images/";
  public $ID, $id;
  public $slug;
  public $owner;
  public $db_data;
  public $init = false;
  public $format="jpeg";
  public $deleteHash;
  public $hash;
  public function __construct($slug) {
    if(is_numeric($slug))
      $img = DB::select("image",array("image_id"=>$slug),array("image_id","DESC"),array(0,1));
    else
      $img = DB::select("image",array("image_slug"=>$slug),array("image_id","DESC"),array(0,1));
    if(count($img)>0) {
      $this->slug = $img[0]["image_slug"]; // Slug
      $this->rel_url = "image/slug/".$this->slug; // relative URL
      $this->abs_url = URL_PATH.$this->rel_url; // //hgfjagkhadgfg.de/image/slug/huhu
      $this->rel_uri = self::$upload_path.$img[0]["path"]; // upload/images/2738127/huhu.jpg
      $this->db_uri  = $img[0]["path"];// 273162381/huhu.jph
      $this->abs_uri = SYSTEM_PATH.self::$upload_path.$img[0]["path"]; // /Aplication/uadhs/htdocs/cathography/upload/images/

      $this->sizes_url = "img/upload/";
      $this->large_url = SYSTEM_PATH.$this->sizes_url."large/".$this->db_uri;
      $this->normal_url = SYSTEM_PATH.$this->sizes_url."normal/".$this->db_uri;
      $this->small_url = SYSTEM_PATH.$this->sizes_url."small/".$this->db_uri;
      $this->medium_url = SYSTEM_PATH.$this->sizes_url."medium/".$this->db_uri;
      $this->fullsz_url = SYSTEM_PATH.$this->sizes_url."fullsz/".$this->db_uri;

      $this->ID = $img[0][0];
      $this->id = $img[0][0];
      if(class_exists("Usero"))
        $this->owner = new Usero($img[0]["user_id"]);
      else $this->owner = $img[0]["user_id"];
      $this->db_data = $img[0];
      $this->format = $img[0]["format"];
      $this->modified = $img[0]["modified"];
      $this->deleteHash = md5($this->ID."deleteit".$this->db_uri);
      $this->hash["delete"] = $this->deleteHash;
      $this->hash["normal"] = hash_file("haval160,4",$this->normal_url);
      if(!empty($img))
        $this->init = true;
      $this->somenumber = (time()%$this->id)*$this->id;
      $imgSize = @getImageSize($this->normal_url);
      $this->res = @number_format(($imgSize[1]/$imgSize[0])*100,2);
    }
  }

  private $color = null;
  private function getColor() {

    if($this->color == null) {

      $filename = $this->medium_url;
      $img = imagecreatefromjpeg($filename);
      $imgSize = getImageSize($filename);
      $imgWidth = $imgSize[0];
      $imgHeight = $imgSize[1];
      $jump[0] = (int)floor(10*($imgWidth/420));
      $jump[1] = (int)floor(10*($imgHeight/420));
      $mod[0] = (int)ceil(25*($imgWidth/420));
      $mod[1] = (int)ceil(25*($imgHeight/420));
      //var_dump($jump,$mod);

      //Farben zählen
      $i=1;
      $red = 0;
      $blue = 0;
      $green = 0;
      if($img) {
        for($x = 0; $x < $imgWidth; $x++){
            for($y = 0; $y < $imgHeight; $y++){
                //$colors ergibt ein Array mit dem imagecolor als Index und dem Zähler als Wert
                $rgb =  imagecolorsforindex($img, imagecolorat($img, $x, $y));
                $red += $rgb['red'];
                $green += $rgb['green'];
                $blue += $rgb['blue'];
                $i++;

                if($y%$mod[1]==0) {
                  $y+=$jump[1];
                }
            }
            if($x%$mod[0]==0) {
              $x+=$jump[0];
            }
        }
        imagedestroy($img);
      }

      $this->color = array("red"=>(int)($red/$i), "green"=>(int)($green/$i), "blue"=>(int)($blue/$i));
    }
    return $this->color;
  }

  public function imgBrightness() {
    $color = $this->getColor();
    $avcolor = (int)(array_sum($color)/3);

    return number_format(($avcolor/256), 2);
  }

  public function averageColor($type="hex",$times=1.0,$add=0) {
    $color = @$this->getColor();

    if($type=="hex") {
      $r = chexform(dechex((float)$times*$color["red"]+$add));
      $g = chexform(dechex((float)$times*$color["green"]+$add));
      $b = chexform(dechex((float)$times*$color["blue"]+$add));

      return strtoupper("#".$r.$g.$b);
    }
    else return $color;
  }

  public function upload_date($format="j.n.Y H:i \U\h\\r") {
    return date($format, strtotime($this->db_data["upload"]));
  }

  public function uploaded() {
    $db = DB::execute("SELECT 1 AS res FROM ".DB_TB_PRE."image_category_link icl
                         JOIN ".DB_TB_PRE."image_category ic USING(image_category_id)
                        WHERE ic.slug = 'uploaded' AND icl.image_id = ".(INT)$this->id);
    return count($db)>0;
  }

  public function access($user=null) {
    if(class_exists("User")) {
      $user = User::$loggedin_user;
    }
    if(class_exists("Usero") && is_numeric($this->owner)) {
      $this->owner = new Usero($this->owner);
    }
    if($this->uploaded()) {
      return true;
    }
    else {
      if($user != null) {
        return ((int)$this->owner->id === (int)$user->id)||$user->admin;
      }
    }
    return false;
  }

}

function chexform($r) {
  if(strlen($r)==1){
    $r = "0".$r;
  }
  else if(strlen($r)<1){
    $r = "00";
  }
  else if(strlen($r)>2){
    $r = "FF";
  }
  return $r;
}
