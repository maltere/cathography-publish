<?php
use entities\Image_Category_Link;
use entities\Image_Category;
use entities\Image;
use Doctrine\ORM\Query\Expr;

class Profilepage extends ppgexmpl {

  protected $module = "image_m";

  public $title = "Bilder";
  public $slug = "image";

  public function __construct() {

    Smarty_i::setTempInRow("profile.tpl","Smarty_display_admin_head");
    Smarty_i::setTempInRow("profile_footer.tpl","Smarty_display_admin_footer");

    $this->pre_function();

    $page["list"] = array("pIndex","Bilderliste");
    $page["add"] = array("pAdd","Hinzufügen");
    $page["review"] = array("pReview","Bilderreview");

    $this->switchPage($page);
    $this->createSubMenu($page);

    Smarty_i::setTempInRow($this->subtemplate,"Smarty_display_admin_main");
  }

  protected function pre_function() {

    if(GV::URIoffset(2) == "upload") {
      $this->upload();
    }

    if(!empty(GV::get("delete")) && !empty(GV::get("id"))) {
      Image_m::remove(GV::get("id"), GV::get("delete"));
    }
  }

  protected function pAdd() {
    $this->subtitle = "Bilder hochladen";
    $this->subtemplate = "admin_image_add.tpl";

    Smarty_i::addJS("js/dropzone.js");
    Smarty_i::addJS("js/drp.js");
  }

  protected function pReview() {
    $this->subtitle = "Review und freischalten";
    $this->subtemplate = "admin_image_review.tpl";

    Smarty_i::setVar("noImagetoReview",false);
    $uploaded = Image_Category::get("uploaded",System::$group);
    $qb = Database::em()->qb();
    $expr = $qb->expr();
    $qb->select("i")->from("entities\Image","i")
       ->leftJoin("i.categories","icl",Expr\Join::WITH, 'icl.category = :cat')
       ->where("i.user = :owner")->andWhere("icl.category IS NULL")->andWhere("i.group = :group");

    $qb2 = $qb;
    $qb2->groupBy("i");
    $qb2->setParameter("owner",Account::$loggedin_user)->setParameter("cat",$uploaded)->setParameter("group",System::$group);

    $image_db = $qb2->getQuery()->getResult();

    if(empty(GV::get("id")) && count($image_db)>0) {
      redirect(URL_PATH."profile/image/review/cat/".GV::get("cat")."/id/".$image_db[0]->getID());
    }
    else if(count($image_db)>0) {
      $qb->andWhere("i.id = :id");
      $qb->setParameter("owner",Account::$loggedin_user)->setParameter("cat",$uploaded)->setParameter("group",System::$group)->setParameter("id",GV::get("id"));
      $ex_id_in_not_uploaded = $qb->getQuery()->getResult();

      if(count($ex_id_in_not_uploaded) == 0)
        redirect(URL_PATH."profile/image/review/cat/".GV::get("cat")."/id/".$image_db[0]->getID());
      else {
        $errors = Image_m::uploadedImgRelease();

        $img = $ex_id_in_not_uploaded[0];
        Smarty_i::setVar("reviewlink",URL_PATH."profile/image/review/cat/".GV::get("cat")."/id/");
        Smarty_i::setVar("reviewError",$errors);
        Smarty_i::setVar("ImageReview",$img);
      }
    }
    else {
      Smarty_i::setVar("noImagetoReview",true);
    }

  }

  // list
  protected function pIndex() {
    $this->subtitle = "Cathography Bilderliste";
    $this->subtemplate = "profile_image_list.tpl";

    $images = Database::em()->findBy("Image",array("group"=>System::$group,"user"=>Account::$loggedin_user));

    smarty_i::setVar("yourimagelist",$images);
  }

  protected function upload() {
    $ret = Image_m::upload();
    if($ret) {
      echo json_encode($ret);
    }
    else {
      SysExc::log(4,"Der Bilderupload ging schief");
      echo json_encode($ret);
    }
    exit();
  }

}
