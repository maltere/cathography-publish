<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

use entities\Image_Category_Link;
use entities\Image_Category;
use entities\Image;

class Image_m extends Module {

  const MODULE_NAME = "image_m";

  public static function __install() {
    Database::em()->updateSchema("Image");
    Database::em()->updateSchema("Image_Category");
    Database::em()->updateSchema("Image_Category_Link");

    if(Database::em()->findOneBy("Image_Category", array("group"=>System::$group,"slug"=>"featured"))==NULL) {
      $featured = new Image_Category(System::$group,"Startseite","featured");
      Database::em()->persist($featured);
    }
    if(Database::em()->findOneBy("Image_Category", array("group"=>System::$group,"slug"=>"uploaded"))==NULL) {
      $uploaded = new Image_Category(System::$group,"Uploaded by","uploaded");
      Database::em()->persist($uploaded);
    }
    $a = Database::em()->find("Image",5);
    Database::em()->flush();

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("required_module", "smarty_i" );
    self::set_config("loadable", true );

  }

  public function __construct() {
    Callback::add_function("System_init_first", "::outputImage","image");
    Callback::add_function("Frontend_page_main", "::image_feed_ajax","image_feed");
    Callback::add_function("Frontend_page_main", "::image_view","image");
    Admin::add_to_menu("image_m","image","Bilder",2);
    Accountmanagement::add_to_menu("image_m","image","Bilder",1);
  }

  public static $sizespath = "img/upload/";
  public static function upload() {
    $ret = false;
    $uploadcheck = true;
    $type = explode("/",$_FILES["file"]["type"]);
    $name = date("Y-m-d-").substr(md5($_FILES["file"]["name"].time()),time()%6,11).".".$type[1];
    $sppath = date("Ym/").$name;
    $uploadpath = Image::$upload_path;
    $image = new Image(System::$group,Account::$loggedin_user,$name,$sppath,$type[1]);
    try {
      Database::em()->persist($image);
      Database::em()->flush();
    }
    catch(Exception $e) {
      $uploadcheck = false;
    }
    if($uploadcheck) {
      self::createDir(rtrim($uploadpath,"/")."/".$sppath);
      if(move_uploaded_file($_FILES["file"]["tmp_name"],rtrim($uploadpath,"/")."/".$sppath)) {
        $ret = true;

        $ret = $ret && self::createPreviews($image->getPath("original"),$image->getPathDB());
      }
      else {
        Database::em()->remove($image);
        $ret = false;
      }
    }
    return $ret;
  }

  public static function uploadedImgRelease() {
    $errors = array();
    if(strlen(GV::get("release"))>0) {
      $uploaded = Image_Category::get("uploaded",System::$group);
      $qb = Database::em()->qb();
      $expr = $qb->expr();
      $qb->select("i")->from("entities\Image","i")
         ->leftJoin("i.categories","icl")
         ->where("i.user = :owner")->andWhere($expr->eq("icl.category",":cat"))->andWhere("i.group = :group");
      $qb->andWhere("i.id = :id");
      $qb->groupBy("i");
      $qb->setParameter("owner",Account::$loggedin_user)->setParameter("cat",$uploaded)->setParameter("group",System::$group)->setParameter("id",GV::get("id"));

      if((int)GV::get("id")<1) {
        $errors[] = array("name"=>false,"msg"=>"Es wurde kein passendes Bild gefunden");
      }
      else if(count($qb->getQuery()->getResult())>0) {
        $errors[] = array("name"=>false,"msg"=>"Das Bild wurde bereits hochgeladen!");
      }
      if(GV::get("cco")!=1) {
        $errors[] = array("name"=>"cco","msg"=>"Die Bilder müssen unter der CC0 Lizenz veröffentlicht werden.");
      }

      //wenn keine errors
      if(count($errors)<1) {
        $image = Database::em()->find("Image",GV::get("id"));
        $uploaded->addImage($image);
        $meta = $image->getMeta();
        $tag = $meta["tag"];
        $newtag = explode(",",GV::get("tag"));
        if(is_array($tag))
          array_merge($tag,$newtag);
        $image->setMeta(array("tag"=>$tag));
        Database::em()->flush();
        $ret =(bool)(count(Database::em()->findBy("Image_Category_Link",array("image"=>$image,"category"=>$uploaded))));
        if($ret)
          redirect(URL_PATH."profile/image/review/cat/".GV::get("cat"));
      }

    }
    return $errors;
  }

  public static function outputImage() {
    if(GV::get("page_slug") == "image" && !empty(GV::get("slug")) && strlen(GV::get("out")) >0 ) {
      $img = self::getImage(GV::get("slug"));
      if($img != NULL && $img->access(Account::$loggedin_user)) {
        $age = 300;
        $lmodified = DateTime::createFromFormat("D, d M Y H:i:s \G\M\T",$allHeaders["If-Modified-Since"]);
        $notmodified = $lmodified>=$img->getModified()&&!empty($lmodified);
        $usual = true; // debug
        if(!$usual) var_dump($notmodified, $lmodified, $lmodified,$img->getModified(),$lmodified<$img->getModified());

        if($notmodified) {
          header($_SERVER["SERVER_PROTOCOL"]." 304 Not Modified",true);
          header('cache-control:private,max-age='.$age);
          header('expires:'.gmdate('D, d M Y H:i:s \G\M\T', time()+$age));
          exit();
        }
        switch (strtolower(GV::get("out"))) {
          case $img->getHash("size","large"):
            $filename = $img->getPath("large");
            break;
          case $img->getHash("size","normal"):
            $filename = ($img->getPath("normal"));
            break;
          case $img->getHash("size","medium"):
            $filename = ($img->getPath("medium"));
            break;
          case $img->getHash("size","small"):
            $filename = ($img->getPath("small"));
            break;
          case $img->getHash("size","fullsz"):
            $filename = ($img->getPath("fullsz"));
            break;
          case $img->getHash("size","original"):
            $filename = ($img->getPath("original"));
            break;
          default:
            exit();
        }
        if(file_exists($filename)) {
          header('cache-control:private,max-age='.$age);
          header('content-type:image/'.$img->getFormat());
          header('content-length: '.filesize($filename));
          header('etag:"'.$img->getHash(),'"');
          header("date:".gmdate('D, d M Y H:i:s \G\M\T', time()));
          header('expires:'.gmdate('D, d M Y H:i:s \G\M\T', time()+$age));
          header("last-modified:".$img->getModified()->format("'D, d M Y H:i:s \G\M\T'"));
          header('pragma:cache');
          header('X-Powered-By:maltereddig.de');
          //header('Server:First you will need to go on a date with me.',true);
          if ($usual) readfile($filename);
        }
        else {
          echo "Bild nicht gefunden.";
        }
        exit;
      }
      else {
        SysExc::log(3,"Das Bild wurde so nicht gefunden.");
      }
    }
  }

  public static function remove($id, $hash) {
    $image = Database::em()->find("Image",$id);
    $ret = false;
    //var_dump($image->getHash("delete") === $hash);
    if($image->getHash("delete") === $hash) {
      @unlink_mul($image->getPath("normal"),
                  $image->getPath("small"),
                  $image->getPath("medium"),
                  $image->getPath("fullsz"),
                  $image->getPath("large") );
      if(!files_exists($image->getPath("normal"),
                  $image->getPath("small"),
                  $image->getPath("medium"),
                  $image->getPath("fullsz"),
                  $image->getPath("large") )) {

          @unlink($image->getPath("original"));

          if(!file_exists($image->getPath("original"))) {
            Database::em()->remove($image);
            Database::em()->flush();
          }
      }
      else {
        self::createPreviews($image->getPath("original"),$image->getPathDB());
        SysExc::log(4, "Die Preview Bilder wurden nicht vollständig gelöscht.");
      }
    }
    return $ret;
  }

  public static function image_view() {
    if(GV::get("page_slug") == "image" && !empty(GV::get("slug"))) {
      $image = self::getImage(GV::get("slug"));
      if(!$image->access(Account::$loggedin_user)) {
        redirect(URL_PATH);
      }
      Frontend::addBodyClass("imageview");
      Smarty_i::setVar("isBright",($image->imgBrightness($image))>0.65);
      Smarty_i::setVar("imageofview",$image);
      Smarty_i::setVar("mainstyle","background-image: url('".$image->getURL("large")."')");
      Smarty_i::setVar("page_subtitle","Photo by ".$image->getUser()->getName());
      Smarty_i::setTempInRow("image_view.tpl","Smarty_display_body_main");
    }
  }

  public static function image_feed_ajax() {
    if(GV::get("page_slug") == "image_feed" && strlen(GV::get("ajax")) >0 ) {
      $ge = explode("&", GV::get("ajax"));
      $ge[2] = empty($ge[2])? "featured": $ge[2];
      if(substr($ge[2], 0, strlen("user=")) === "user=") {
        $tmp = explode("=",$ge[2]);
        $ge[2] = Database::em()->findOneBy("entities\User",array("username"=>$tmp[1]));
      }
      $r = self::feed($ge[0],$ge[1],$ge[2],false);
      if($r > 0)
        Smarty_i::display("image_feed.tpl");
      else {
        echo 0;
      }
      exit;
    }
  }

  public static function feed($page=1, $render=4, $slug="featured", $all=true) {
    $page = $page<1? 1: $page;
    $render = $render<1? 4: $render;
    $c = 0;

    if($slug instanceof entities\User) {
      Smarty_i::setVar("feedtype","user=".$slug->getUsername());
    }
    else {
      Smarty_i::setVar("feedtype",$slug);
    }

    if($all) {
      for($i=1;$i<=$page;$i++) {
        $c+=count(self::feedSetVar($i,$render,$slug));
      }
    }
    else {
      $c+=count(self::feedSetVar($page,$render,$slug));
    }

    //Smarty_i::setVar("ImageFeedPage",$page);
    Smarty_i::setTempInRow("image_feed.tpl","Smarty_display_body_main");
    return $c;
  }

  private static function feedSetVar($page, $render, $slug) {
    if($slug instanceof entities\User) {
      $featured = self::getImagesByUser($slug,$page,$render);
    }
    else {
      $featured = self::getImagesByCatSlug($slug, $page, $render);
    }
    $feedvar = Smarty_i::getVar("ImageFeed");
    $feedvar[$page] = $featured;
    ksort($feedvar);
    Smarty_i::setVar("ImageFeed",$feedvar);
    return $featured;
  }

  public static function getImage($slug) {
    $image = Database::em()->findOneBy("Image",array("slug"=>$slug,"group"=>System::$group));;
    return $image;
  }

  public static function createPreviews($source,$db_uri) {
    $restore = ini_get('memory_limit');
    $destination_path = self::$sizespath;
    ini_set('memory_limit','512M');
    $ret = self::compressImage($source,$destination_path."fullsz/".ltrim($db_uri,"/"),60);
    if(file_exists($destination_path."fullsz/".ltrim($db_uri,"/"))) {
      $ret = $ret && self::createResizedImage($source,$destination_path."large/".ltrim($db_uri,"/"),60,2000,1500);
    }
    else {
      SysExc::log(403,"Bildgröße fullsz konnte nicht erstellt werden.");
      return false;
    }
    if(file_exists($destination_path."large/".ltrim($db_uri,"/"))) {
      $ret = $ret && self::createResizedImage($source,$destination_path."normal/".ltrim($db_uri,"/"),70,1270,900);
    }
    else {
      SysExc::log(403,"Bildgröße large konnte nicht erstellt werden.");
      return false;
    }
    if(file_exists($destination_path."normal/".ltrim($db_uri,"/"))) {
      $ret = $ret && self::createResizedImage($source,$destination_path."medium/".ltrim($db_uri,"/"),50,850,400);
    }
    else {
      SysExc::log(403,"Bildgröße normal konnte nicht erstellt werden.");
      return false;
    }
    if(file_exists($destination_path."medium/".ltrim($db_uri,"/"))) {
      $ret = $ret && self::createResizedImage($source,$destination_path."small/".ltrim($db_uri,"/"),40,200,100);
    }
    else {
      SysExc::log(403,"Bildgröße medium konnte nicht erstellt werden.");
      return false;
    }
    if(!file_exists($destination_path."small/".ltrim($db_uri,"/"))) {
      SysExc::log(403,"Bildgröße small konnte nicht erstellt werden.");
      return false;
    }
    ini_set('memory_limit',$restore);
    return $ret;
  }

  public static function createDir($dir) {

    $some = explode("/",$dir);
    $file = array_pop($some);
    $path = join("/",$some);
    if(!file_exists($path))
      mkdir($path,0777,true);

  }

  public static function createResizedImage($source_url, $destination_url, $quality, $width, $height) {
    self::createDir($destination_url);
    // Get new dimensions
    list($width_orig, $height_orig) = getimagesize($source_url);

    $ratio_orig = $width_orig/$height_orig;

    if ($width/$height > $ratio_orig) {
       $width = $height*$ratio_orig;
    } else {
       $height = $width/$ratio_orig;
    }

    // Resample
    $image_p = imagecreatetruecolor($width, $height);
    $image = imagecreatefromjpeg($source_url);
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
    imagejpeg($image_p,$destination_url,$quality);
    imagedestroy($image_p);
    imagedestroy($image);
    return $destination_url;
  }

  public static function compressImage($source_url, $destination_url, $quality) {
    self::createDir($destination_url);
    //$quality :: 0 - 100
    $info = getimagesize($source_url);

    if ($info['mime'] == 'image/jpeg')
    {
        $image = imagecreatefromjpeg($source_url);
        //save file
        imagejpeg($image, $destination_url, $quality);//ranges from 0 (worst quality, smaller file) to 100 (best quality, biggest file). The default is the default IJG quality value (about 75).
        //Free up memory
        imagedestroy($image);
    }
    elseif ($info['mime'] == 'image/png')
    {
        $image = imagecreatefrompng($source_url);

        imageAlphaBlending($image, true);
        imageSaveAlpha($image, true);

        /* chang to png qulity */
        $png_quality = 9 - (($quality * 9 ) / 100 );
        imagePng($image, $destination_url, $png_quality);//Compression level: from 0 (no compression) to 9.
        //Free up memory
        imagedestroy($image);
    }

    //return destination file
    return $destination_url;
  }

  public static function getImagesByCatSlug($slug,$page=1,$render=10,$reviewed=1) {
    $qb = Database::em()->qb();
    $qb->select("icl")->from("entities\Image_Category_Link","icl")->join("icl.category","ic")->join("icl.image","i")
       ->where("icl.group = :group")->andWhere("ic.slug = :slug")->orderBy("i.upload","DESC")
       ->setFirstResult( ($page-1)*$render )
       ->setMaxResults( $render );
    $qb->setParameter("group",System::$group);
    $qb->setParameter("slug", $slug);
    $image_db = $qb->getQuery()->getResult();
    $image = array();
    foreach($image_db as $key=>$value) {
      array_push($image, $value->getImage());
    }
    return $image;
  }

  public static function getImagesByUser($slug,$page=1,$render=10,$reviewed=1) {
    $qb = Database::em()->qb();
    $qb->select("icl")->from("entities\Image_Category_Link","icl")->join("icl.category","ic")->join("icl.image","i")
       ->where("icl.group = :group")->andWhere("i.user = :user")->andWhere("ic.slug = :slug")->orderBy("i.upload","DESC")
       ->setFirstResult( ($page-1)*$render )
       ->setMaxResults( $render );
    $qb->setParameter("group",System::$group);
    $qb->setParameter("slug", "uploaded");
    $qb->setParameter("user", $slug);
    $image_db = $qb->getQuery()->getResult();
    $image = array();
    foreach($image_db as $key=>$value) {
      array_push($image, $value->getImage());
    }
    return $image;
  }
}
