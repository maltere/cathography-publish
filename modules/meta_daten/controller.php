<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

use entities\Group_Setting;

class Meta_daten extends Module {

  const MODULE_NAME = "meta_daten";

  public static function __install() {

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("loadable", true );

  }

  public function __construct() {
    self::setDefaultIfNot("page_lang","de","lang");
    self::setDefaultIfNot("page_description","Standard","description");
    self::setDefaultIfNot("page_title","Erste Seite","title");
    self::setDefaultIfNot("page_author","Malte Reddig","author");

    Callback::add_function("System_init_first", "::setMetaDatenCallback");
  }

  public static function setVar($name, $val, $meta = NULL, $module = "meta_daten") {

      $return = Database::em()->findOneBy("Group_Setting",array("group"=>System::$group,"var"=>$name,"module"=>$module));
      if($val == NULL) {
        if($return != NULL)
          Database::em()->remove($return);
      }
      else {
        if($return == NULL) {
          $return = new Group_Setting(System::$group,$name,$val);
          Database::em()->persist($return);
        }
        else {
          $return->setValue($val);
        }
        $return->setModule($module);
        if($meta != NULL)
          $return->setMeta($meta);
      }
      Database::em()->flush();
  }

  public static function getVar($name, $module=NULL,$field=false) {
    if($module!=NULL) {
      $return = Database::em()->findOneBy("Group_Setting",array("group"=>System::$group,"var"=>$name,"module"=>$module));
    }
    else {
      $return = Database::em()->findOneBy("Group_Setting",array("group"=>System::$group,"var"=>$name));
    }

    if($field !== false && $return != NULL) {
      switch (strtolower($field)) {
        case "value": return $return->getValue(); break;
        case "val": return $return->getValue(); break;

        case "module": return $return->getModule(); break;

        case "name": return $return->getName(); break;

        case "meta": return $return->getMeta(); break;
      }
    }
    return $return;
  }
  public static function get($name,$module=NULL) {
    return self::getVar($name,$module);
  }

  public static function setDefaultIfNot($name,$val,$meta=NULL,$module="meta_daten") {
      if(count(Database::em()->findOneBy("Group_Setting", array("group"=>System::$group,"var"=>$name)))<1) {
        $dbvar = new Group_Setting(System::$group,$name,$val);
        $dbvar->setMeta($meta);
        $dbvar->setModule($module);

        Database::em()->persist($dbvar);
        Database::em()->flush();
      }
  }

  public static function setMetaDatenCallback() {

    $return = Database::em()->findBy("Group_Setting", array("group"=>System::$group,"module"=>"meta_daten"));

    $set = array();
    foreach($return as $res) {
      $set[$res->getMeta()] = $res->getValue();
    }

    Smarty_i::setVar("header", $set );
  }
}
