<?php

class Adminpage extends apgexmpl {

  protected $module = "admin";

  public $title = "Adminverwaltung";
  public $slug = "admin";

  public function __construct() {

    parent::__construct();

    Smarty_i::setTempInRow("admin.tpl","Smarty_display_admin_head");
    Smarty_i::setTempInRow("admin_footer.tpl","Smarty_display_admin_footer");

    $this->pre_function();

    $this->switchPage($page);
    $this->createSubMenu($page);

    Smarty_i::setTempInRow($this->subtemplate,"Smarty_display_admin_main");
  }

  protected function pre_function() {
  }

  protected function pIndex() {

  }

}
