<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

require_once __DIR__."/apgexmpl.php";

class Admin extends Module {

  const MODULE_NAME = "admin";

  public static function __install() {

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("required_module", "menu_container" );
    self::set_config("required_module", "account" );
    self::set_config("loadable", true );
  }

  public function __construct() {
    Callback::add_function("Frontend_page_main", "::initPage", "admin");

    Admin::add_to_menu("admin","admin","Adminstart",0);
    Admin::add_to_menu("account","logout","Logout",100,0,null,true);
  }

  protected static $menu = array(array(),array());
  protected static $modulemenu;
  public static function add_to_menu($module,$slug,$name,$rank=0,$menu=0,$css=null,$abs_path=false) {
    $slug = strtolower($slug);
    if(count(self::$menu[(int)$menu][$rank])>0) {
      self::add_to_menu($module,$slug,$name,$rank+1,$menu,$css,$abs_path);
      return;
    }
    if($abs_path !== false) $path = $slug;
    else {
      $path = "admin/".$slug;
    }
    self::$menu[(int)$menu][$rank] = array("slug"=>$slug, "path"=>$path,"title"=>$name, "abs_path"=>($abs_path!==false), "css"=>$css);
    self::$modulemenu[(int)$menu][$slug][0] = $module;
    self::$modulemenu[(int)$menu][$slug][1] = $slug;
  }

  public static $pageObj;
  public static function initPage() {
    if(GV::get("page_slug")=="admin" && Account::loggedin() && Account::$loggedin_user->isAdmin()) {
      $page = GV::URIoffset(1);
      if($page == NULL) {
        $page = "admin";
      }
      if( strlen(self::$modulemenu[0][$page][0])>0 &&
          strtolower(self::$modulemenu[0][$page][0]) != strtolower($page)) {
        $page = strtolower(self::$modulemenu[0][$page][0]);
      }
      $admin = strtolower($admin);
      $module = Module::$loaded_module;
      $url = rtrim($module[$page]["path"],"/")."/adminpage.php";
      if(file_exists($url))
        require_once $url;
      else
        redirect(URL_PATH);
      self::$pageObj = new Adminpage();
      Callback::add_function("Smarty_display_body_main", "::displayAdminPage");
    }
    else if(GV::get("page_slug")=="admin") {
      redirect(URL_PATH);
    }
  }

  public static function displayAdminPage() {
    ksortRecursive(self::$menu);
    Smarty_i::setVar("adminmenu",self::$menu);
    Smarty_i::setVar("adminpage",self::$pageObj);
    self::$pageObj->display();
  }
}
