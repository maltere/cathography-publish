<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

require_once SYSTEM_PATH."smarty/libs/Smarty.class.php";
//require_once __DIR__."/config.php";

use entities\Menu;
use entities\Menu_Item;

class Menu_Container extends Module {

  const MODULE_NAME = "menu_container";

  public static function __install() {
    Database::em()->updateSchema("Menu");
    Database::em()->updateSchema("Menu_Item");
  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("loadable", true );


    if(Database::em()->findOneBy("Menu", array("group"=>System::$group,"slug"=>"mainmenu"))==NULL) {
      $mainmenu = new Menu(System::$group,"Mainmenü","mainmenu");
      $mainmenu->addItem("Startseite","/");
      $mainmenu->addItem("Admin","/admin/");
      Database::em()->persist($mainmenu);
    }
    Database::em()->flush();
  }

  public function __construct() {
    self::get_menus();
    self::set_menu_vars();
  }

  protected static $menus = array();
  public static function get_menu_id($slug) {
    $menu = Menu::get($slug);
    return $menu->getID();
  }
  public static function get_menus() {
    if(empty(self::$menus) || $force)
      self::$menus = Database::em()->findBy("Menu",array("active"=>True));
    //SysExc::log(3,self::$menus);
    return self::$menus;
  }

  public static $menu_Vars = array();
  public static function set_menu_vars() {
    foreach(self::$menus as $menu) {
      self::$menu_Vars[$menu->getSlug()] = $menu->getItems();
    }

  }
}
