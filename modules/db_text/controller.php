<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/


class Db_text extends Module {

  const MODULE_NAME = "db_text";

  public static function __install() {
    Database::em()->updateSchema("DB_Text");
  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", false );
    self::set_config("loadable", true );

  }

  public function __construct() {
  }

  public static function setVar($name, $val, $force = false) {
    if($force)
      $return = new entities\DB_Text(System::$group,$name,$val);
    else {
      $return = Database::em()->findOneBy("entities\DB_Text",array("slug"=>$name));
      if($return == NULL) {
        $return = new entities\DB_Text(System::$group,$name,$val);
        Database::em()->persist($return);
      }
      else {
        $return->setValue($val);
      }
    }


    Database::em()->flush();


    return $return;
  }

  public static function getVar($name) {
    $return = Database::em()->findOneBy("DB_Text",array("slug"=>$name));
    if($return != NULL)
      return $return->getValue();
    return "";
  }
}
