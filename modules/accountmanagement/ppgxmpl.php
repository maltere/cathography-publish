<?php
class ppgexmpl {
  public $subtitle;
  public $subslug;

  public $subtemplate = "profile_std.tpl";

  public function __construct() {
  }

  protected function switchPage($pages) {
    if(!empty($pages)) {
      foreach($pages as $slug=>list($fu,$na)) {
        if(strtolower(GV::URIoffset(2))==strtolower($slug)) {
          $this->subslug = $slug;
          if($fu!=NULL)
            $this->$fu();
          return;
        }
      }
    }
    $this->pIndex();
  }

  protected function createSubMenu($pages) {
    if(!empty($pages)) {
      $i=0;
      foreach($pages as $slug => list($fu,$na)) {

        Accountmanagement::add_to_menu($this->module,$slug,$na,$i,1);
        $i++;
      }
    }
  }

  public function display() {
    Callback::init("Smarty_display_admin_head");
    Callback::init("Smarty_display_admin_main");
    Callback::init("Smarty_display_admin_footer");
  }

  public function getURL($sub) {
    $r = "";
    $r .= URL_PATH."profile/".$this->slug."/";
    if(strlen($this->subslug)>0 && (bool)$sub) $r .= $this->subslug."/";
    return $r;
  }

}
