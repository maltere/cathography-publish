<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

require_once __DIR__."/ppgxmpl.php";

class Accountmanagement extends Module {

  const MODULE_NAME = "accountmanagement";

  public static function __install() {

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("required_module", "account" );
    self::set_config("loadable", true );
  }

  public function __construct() {
    Callback::add_function("Frontend_page_main", "::initPage", "profile");

    Accountmanagement::add_to_menu("accountmanagement","index","Profil",0);
    Accountmanagement::add_to_menu("account","logout","Logout",100,0,null,true);
  }

  protected static $menu = array(array(),array());
  protected static $modulemenu;
  public static function add_to_menu($module,$slug,$name,$rank=0,$menu=0,$css=null,$abs_path=false) {
    $slug = strtolower($slug);
    if(count(self::$menu[(int)$menu][$rank])>0) {
      self::add_to_menu($module,$slug,$name,$rank+1,$menu,$css,$abs_path);
      return;
    }
    if($abs_path !== false) $path = $slug;
    else {
      $path = "profile/".$slug;
    }
    self::$menu[(int)$menu][$rank] = array("slug"=>$slug, "path"=>$path,"title"=>$name, "abs_path"=>($abs_path!==false), "css"=>$css);
    self::$modulemenu[(int)$menu][$slug][0] = $module;
    self::$modulemenu[(int)$menu][$slug][1] = $slug;
  }

  public static $pageObj;
  public static function initPage() {
    if(GV::get("page_slug")=="profile" && Account::loggedin()) {
      $page = GV::URIoffset(1);
      if($page == NULL) {
        $page = "index";
      }
      if( strlen(self::$modulemenu[0][$page][0])>0 &&
          strtolower(self::$modulemenu[0][$page][0]) != strtolower($page)) {
        $page = strtolower(self::$modulemenu[0][$page][0]);
      }
      $admin = strtolower($admin);
      $module = Module::$loaded_module;
      $url = rtrim($module[$page]["path"],"/")."/profilepage.php";
      if(file_exists($url))
        require_once $url;
      else
        redirect(URL_PATH);
      self::$pageObj = new Profilepage();
      Callback::add_function("Smarty_display_body_main", "::displayProfilePage");
    }
    else if(GV::get("page_slug")=="profile") {
      redirect(URL_PATH);
    }
  }

  public static function displayProfilePage() {
    ksortRecursive(self::$menu);
    Smarty_i::setVar("adminmenu",self::$menu);
    Smarty_i::setVar("adminpage",self::$pageObj);
    self::$pageObj->display();
  }
}
