<?php

class Profilepage extends ppgexmpl {

  protected $module = "accountmanagement";

  public $title = "Account";
  public $slug = "index";

  public function __construct() {

    Smarty_i::setTempInRow("profile.tpl","Smarty_display_admin_head");
    Smarty_i::setTempInRow("profile_footer.tpl","Smarty_display_admin_footer");

    $this->pre_function();

    $page["changepw"] = array("pPasswordchange","Kennwort ändern");

    $this->switchPage($page);
    $this->createSubMenu($page);

    Smarty_i::setTempInRow($this->subtemplate,"Smarty_display_admin_main");
  }

  protected function pre_function() {
  }

  protected function pIndex() {
    $this->subtemplate = "profile_account.tpl";
  }

  protected function pPasswordchange() {
    $this->subtemplate = "profile_pw_change.tpl";
    $this->subtitle = "Passwort ändern";

    if((int)GV::get("edit","GET")===1) {
      if(GV::get("npw","POST")===GV::get("npwwdh","POST")) {
        $return = Account::$loggedin_user->changePassword(GV::get("oldpw","POST"),GV::get("npw","POST"));
        Database::em()->flush();
        if($return) {
          Smarty_i::setVar("changePW",array(true,"notice"));
        }
        else {
          Smarty_i::setVar("changePW",array(true,"error"));
        }
      }
    }
  }

}
