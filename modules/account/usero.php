<?php

class Usero {
  private $db_data;
  public $session_salt;
  public $id,$ID;
  public $admin;
  public function __construct($s) {
    if(is_numeric($s)) $s = self::getUsernameByID($s);
    $tmp = DB::select("user",array("username"=>$s),array("user_id","ASC"),array(0,1));
    $this->db_data = $tmp[0];
    $this->id = $tmp[0]["user_id"];
    $this->ID = $this->id;
    $this->admin = (bool)$tmp[0]["admin"];

  }
  public static function getUsernameByID($id) {
    $u = DB::select("user",array("user_id"=>(int)$id),array("user_id","ASC"),array(0,1),array("username"));
    return $u[0][0];
  }

  public function get($v) {
    if($v == "pw") return null;
    else if($v == "spw") return null;
    return $this->db_data["$v"];
  }

  public function name($f = "username") {
    if($f == "fullname")
      return $this->db_data["firstname"]." ".$this->db_data["lastname"];
    else if($f == "firstname")
      return $this->db_data["firstname"];
    else if($f == "lastname")
      return $this->db_data["lastname"];
    else if($f == "username")
      return $this->db_data["username"];
    return "";
  }
}
