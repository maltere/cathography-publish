<?php

class Adminpage extends apgexmpl {

  protected $module = "account";

  public $title = "Account";
  public $slug = "account";

  public function __construct() {

    Smarty_i::setTempInRow("admin.tpl","Smarty_display_admin_head");
    Smarty_i::setTempInRow("admin_footer.tpl","Smarty_display_admin_footer");

    $this->pre_function();

    $page["list"] = array("pList","Nutzerliste");
    $page["add"] = array("pAdd","Hinzufügen");

    $this->switchPage($page);
    $this->createSubMenu($page);

    Smarty_i::setTempInRow($this->subtemplate,"Smarty_display_admin_main");
  }

  protected function pre_function() {

    if(!empty(GV::get("delete")) && !empty(GV::get("id"))) {
      Account::remove(GV::get("id"), GV::get("delete"));
    }
  }

  protected function pList() {
    $this->subtitle = "Accountliste";
    $this->subtemplate = "admin_user_list.tpl";

    $user_row = Database::em()->findBy("User",array());
    Smarty_i::setVar("userrow",$user_row);
  }

  protected function pAdd() {
    $this->subtitle = "Account hinzufügen";
    $this->subtemplate = "admin_user_add.tpl";

    if(GV::get("account") == "add" && GV::get("now")) {
      if (!empty(GV::get("username","POST")) && filter_var(GV::get("email"), FILTER_VALIDATE_EMAIL) && !empty(GV::get("kennwort","POST")) && !empty(GV::get("firstname","POST")) && !empty(GV::get("lastname","POST"))) {
        if(GV::get("kennwort") === GV::get("passwordw")) {
          $ret = Account::addAccount(GV::get("username"),GV::get("email"),GV::get("firstname"),GV::get("lastname"),GV::get("kennwort"));
          Smarty_i::setVar("addAccount",array(true,GV::get("username"),"notice"));
          return;
        }
      }
      Smarty_i::setVar("addAccount",array(true,GV::get("username"),"error"));
    }
  }

  protected function pIndex() {

  }

}
