<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/
session_start();
use entities\User as User;
use entities\User_Log;

class Account extends Module {

  const MODULE_NAME = "account";

  public static function __install() {
    Database::em()->updateSchema("User");
    Database::em()->updateSchema("User_Log");
  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("loadable", true );
    self::set_config("required_module", "accountmanagement" );
    self::set_config("require_module", "email" );

    $qb = Database::em()->qb();
    $qb->select("u")->from("entities\User","u")->join("u.groups","g")
       ->where("u.admin = True")->andWhere("g.id = :group");
    $qb->setParameter("group",System::$group);
    $r = $qb->getQuery()->getOneOrNullResult();
    if($r==NULL) {
      $ret = self::addAccount("admin","mr@kamlage-reddig.de","Malte","Reddig","start",1);
    }
  }

  public function __construct() {
    Callback::add_function("System_init", "::doLogin");
    Callback::add_function("System_init", "::doRegister");
    Callback::add_function("Frontend_page_main", "::loginPage", "login");
    Callback::add_function("Frontend_page_main", "::logoutPage", "logout");
    Callback::add_function("Frontend_page_main", "::registerPage", "register");
    Admin::add_to_menu("account","account","Accountverwaltung",1);
    Account::loggedin();
  }

  public static function doLogin() {
    if (GV::get("page_slug") == "login" && GV::get("user") && !empty(GV::get("username","POST"))) {
      Account::login(GV::get("username","POST"),GV::get("kennwort","POST"));
    }
    $re = Account::log();
  }

  public static function doRegister() {
    $post = GV::get("POST","k");
    //var_dump(GV::get());
    if (GV::get("page_slug") == "register" && GV::get("user") && !empty(GV::get("username","POST")) && filter_var(GV::get("email"), FILTER_VALIDATE_EMAIL) && !empty(GV::get("kennwort","POST")) && !empty(GV::get("firstname","POST")) && !empty(GV::get("lastname","POST"))) {
      if(GV::get("kennwort") === GV::get("passwordw") && GV::get("agb") == 1) {
        $ret = Account::addAccount(GV::get("username"),GV::get("email"),GV::get("firstname"),GV::get("lastname"),GV::get("kennwort"));
        if($ret) {
          Account::login(GV::get("username","POST"),GV::get("kennwort","POST"));
          $re = Account::log();
        }
      }
    }
  }

  public static function addAccount($username, $email, $firstname, $lastname, $password,$admin=0) {
    if(count(Database::em()->findOneBy("entities\User",array("email"=>$email)))>0) {
      SysExc::log(609,"Email schon benutzt");
    }
    else if(count(Database::em()->findOneBy("entities\User",array("username"=>$username)))>0) {
      SysExc::log(610,"Username schon benutzt");
    }
    else {
      try {
        $user = new User($username,$email);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setPassword($password);
        if((bool)$admin)
          $user->makeAdmin();
        $user->addGroup(System::$group);
        Database::em()->persist($user);
        Database::em()->flush();
        $email = Email::init();
        $email->setUser($user);
        $email->setSubject("Dein Account bei ".Meta_daten::get("page_title")->getValue()." muss nur noch aktiviert werden.");
        $email->setMessage("Dein Code: ".$user->getHash("register"),false);
        $email->prepareEmail();
        $email->send();
        return true;
      }
      catch(Exception $e) {
        return false;
      }
    }

  }

  public static function remove($id, $hash) {
    $user = Database::em()->find("User",$id);
    $ret = false;
    if($user->getHash("delete") === $hash) {
      Database::em()->remove($user);
      Database::em()->flush();
    }
    return true;
  }

  public static function createSalt() {
    return utf8_encode( mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
  }

  public static function createHash($pw,$salt,$easy=false) {
    if(!empty($pw) && !empty($salt)) {
      $options = [
        'cost' => 11,
        //'salt' => $salt,
      ];
      if($easy) {
        $ret = utf8_encode(hash("sha256", $pw.crypt($pw,$salt)));
        return $ret;
      }
      else
        return utf8_encode(password_hash($pw, PASSWORD_BCRYPT, $options));
    }
    return false;
  }

  public static function createSession() {
    $browser = get_browser_properties();
    $ssid = session_id();
    $u = self::$loggedin_user->getUsername();
    $s = self::createSalt();
    $h = self::createHash($ssid,$s);
    $o = self::createHash(md5($browser["browser"]).$h.$u,$s,true);
    $b = setcookie(DB_TB_PRE."hresu",$h,0,"/");
    $c = setcookie(DB_TB_PRE."oresu",$o,0,"/");
    if($b&&$c) {
      self::$loggedin_user->addActivity(System::$group,$h,$s,$_SERVER["REQUEST_URI"]);
      Database::em()->flush();
    }
    $b = $b&&$c;
    return $b;
  }

  public static $loggedin_user = false;
  public static function login($username,$password) {
    if(!self::loggedin()) {
      $u = Database::em()->findOneBy("User",array("username"=>$username),array("id"=>"ASC"));

      if($u instanceof User) {
        $salt = $u->getSalt();
        if($u->checkPassword($password)) {
          self::$loggedin_user = $u;
          $res = self::createSession();
          if($res === false) {
            return false;
          }
          redirect($_SERVER["REQUEST_URI"]);

          return true;
        }
      }
      SysExc::log(6,"User oder Passwort stimmt nicht");

      return false;
    }
  }
  public static function logout() {
    if(self::loggedin()) {
      $b = setcookie(DB_TB_PRE."hresu","",1,"/");
      $c = setcookie(DB_TB_PRE."oresu","",1,"/");
      self::$loggedin_user == false;
      return $b&&$c&&(self::$loggedin_user===false);
    }
  }

  public static function log() {
    Callback::add_function("System_init_second","::reschedulelog");
    return true;
  }
  public static function reschedulelog() {
    if(self::loggedin()) {
      $h = $_COOKIE[DB_TB_PRE."oresu"];
      $s = self::$loggedin_user->getSessionSalt();
      return self::$loggedin_user->addActivity(System::$group,$h,$s,$_SERVER["REQUEST_URI"]);
    }
    return false;
  }

  public static function loggedin() {
    if(self::$loggedin_user == false && !empty($_COOKIE[DB_TB_PRE."oresu"])) {
      $o = $_COOKIE[DB_TB_PRE."oresu"];
      $h = $_COOKIE[DB_TB_PRE."hresu"];
      $re = User_Log::get($h);
      if(!empty($re)) {
        $browser = get_browser_properties();
        $s = $re[0]->getSalt();
        $u = $re[0]->getUser()->getUsername();
        $i = self::createHash(md5($browser["browser"]).$h.$u,$s,true);
        if($o === $i) {
          self::$loggedin_user = $re[0]->getUser();
          Callback::add_function("Smarty_display_head_first","Smarty_i::setVar",array("loggedin_user",self::$loggedin_user),true);
          self::$loggedin_user->setSessionSalt($s);
          return true;
        }
      }
    }
    else if (self::$loggedin_user instanceof User){
      Smarty_i::setVar("loggedin",true);
      return true;
    }
    Smarty_i::setVar("loggedin",false);
    return false;
  }

  public static function logoutPage() {
    if(GV::get("page_slug") == "logout") {
      //redirect(URL_PATH."login/");

      if(Account::logout() ) {
        redirect(URL_PATH."login");
      }
      else if(Account::$loggedin_user!= NULL && !Account::$loggedin_user->isAdmin() ) {
        redirect(URL_PATH."profile");
      }
      else {
        redirect(URL_PATH."admin");
      }
    }
  }

  public static function loginPage() {
    if(GV::get("page_slug") == "login") {

      if(Account::loggedin() && Account::$loggedin_user->isAdmin()) {
        redirect(URL_PATH."admin");
      }
      else if(Account::loggedin() ) {
        redirect(URL_PATH."profile");
      }
      else {
        Smarty_i::setTempInRow("login.tpl","Smarty_display_body_main");
      }
    }
  }

  public static function registerPage() {
    if(GV::get("page_slug") == "register") {

      if(Account::loggedin() ) {
        redirect(URL_PATH."admin");
      }
      else {
        Smarty_i::setTempInRow("register.tpl","Smarty_display_body_main");
      }
    }
  }
}
