<?php

use entities\User;

class Email extends Module {

  const MODULE_NAME = "email";

  public static function __install() {
    Database::em()->updateSchema("Email_Log");
  }

  public static function __config() {
    self::set_config("force_load", false );
    self::set_config("loadable", true );
  }

  public function __construct() {
    $this->from = new User("system",SYSTEM_EMAIL);
    $this->from->setName(Meta_daten::get("page_title")->getValue(),"Systemmail");
  }

  public static function init() {
    return new Email();
  }

  protected $too;
  public function setEmail($email) {
    $user = Database::em()->findOneBy("entities\User",array("email"=>$email));
    $this->too = $user;
  }
  public function setUser(User $user) {
    $this->too = $user;
  }
  protected $message;
  protected $message_text_file;
  public function setMessage($message, $isTemplate = true) {
    if($isTemplate) {
      if(file_exists(Smarty_i::CONTENT_TEMP_PATH."/".$message)) {
        $this->message_text_file = $message;
      }
    }
    else {
      $this->message = $message;
    }
  }
  protected $from;
  public function setFrom(User $from) {
    $this->from = $from;
  }
  protected $subject;
  public function setSubject($subject) {
    $this->subject = $subject;
  }

  protected function getFrom() {
    return trim($this->from->getName())." <".$this->from->getEmail().">";
  }

  protected function getToo() {
    return trim($this->too->getName())." <".$this->too->getEmail().">";
  }

  public function prepareEmail() {
    if(!filter_var($this->from->getEmail(), FILTER_VALIDATE_EMAIL)) {
      return false;
    }
    if(!filter_var($this->too->getEmail(), FILTER_VALIDATE_EMAIL)) {
      return false;
    }
    if(strlen($this->message)<1 && !file_exists(Smarty_i::CONTENT_TEMP_PATH."/".$this->message_text_file)) {
      return false;
    }

    $this->smarty = Smarty_i::$smarty;

    if(file_exists(Smarty_i::CONTENT_TEMP_PATH."/".$this->message_text_file) && strlen($this->message_text_file)>0) {
      $this->smarty->assign("include_a_file",true);
      $this->smarty->assign("include_this_file",$this->message_text_file);
    }
    else {
      $this->smarty->assign("include_a_file",false);
      $this->smarty->assign("okay_than_a_message_here",$this->message);
    }


    return true;
  }
  public function send() {
    $content = $this->smarty->fetch("email_template.tpl");
    $header  = "MIME-Version: 1.0\r\n";
    $header .= "Content-type: text/html; charset=utf-8\r\n";;
    $header .= "From: ".$this->getFrom()."\r\n";
    $header .= "Reply-To: ".$this->getFrom()."\r\n";
    $header .= "X-Mailer: PHP ". phpversion();

    $ret = mail($this->getToo(),$this->subject,$content,$header);
    $maillog = new entities\Email_Log(System::$group,$this->too,$this->subject,$ret);
    Database::em()->persist($maillog);
    Database::em()->flush();
    return $ret;
  }

}

 ?>
