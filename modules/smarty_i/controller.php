<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

require_once SYSTEM_PATH."smarty/libs/Smarty.class.php";
require_once __DIR__."/config.php";

class Smarty_i extends Module {

  const MODULE_NAME = "smarty_i";
  const CONTENT_PATH          = SYSTEM_PATH."content";
  const CONTENT_TEMP_PATH     = SYSTEM_PATH."content/templates";
  const CONTENT_TEMP_C_PATH   = SYSTEM_PATH."content/templates_c";
  const CONTENT_CACHE_PATH    = SYSTEM_PATH."content/cache";
  const CONTENT_CONFIGS_PATH  = SYSTEM_PATH."content/configs";

  public static $smarty;
  public static $stop = false;
  private static $templates = array();
  public static $javascript = array();
  public static $externaljavascript = array();
  private static $css = array();
  private static $externalcss = array();

  public static function __install() {

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("loadable", true );

    self::$smarty = new Smarty();
    self::$smarty->setCaching(Smarty::CACHING_LIFETIME_SAVED);
    self::$smarty->setCacheLifetime(SMARTY_CACHE_LIFETIME);
    self::$smarty->caching = !(DEBUG);

    if(!is_dir(  self::CONTENT_PATH  )  )
      mkdir(  self::CONTENT_PATH  );

    if(!is_dir(  self::CONTENT_TEMP_PATH  )  )
      mkdir(  self::CONTENT_TEMP_PATH  );

    if(!is_dir(  self::CONTENT_TEMP_C_PATH  )  )
      mkdir(  self::CONTENT_TEMP_C_PATH  );

    if(!is_dir(  self::CONTENT_CACHE_PATH  )  )
      mkdir(  self::CONTENT_CACHE_PATH  );

    if(!is_dir(  self::CONTENT_CONFIGS_PATH  )  )
      mkdir(  self::CONTENT_CONFIGS_PATH  );

    self::$smarty->setTemplateDir(  self::CONTENT_TEMP_PATH     );
    self::$smarty->setCompileDir(   self::CONTENT_TEMP_C_PATH   );
    self::$smarty->setCacheDir(     self::CONTENT_CACHE_PATH    );
    self::$smarty->setConfigDir(    self::CONTENT_CONFIGS_PATH  );

  }

  public function __construct() {
    Callback::add_function("System_init_third", "::displayCallback",null,999);

    self::setSystemVar();
  }

  public static function setSystemVar() {
    $system = array(
      "url"  => HTDOCS_URI,
      "page_url" => $_SERVER["REQUEST_URI"]
    );

    Smarty_i::setVar("system",$system);
  }

  public static function displayCallback() {
    self::setVar("JavaScript", self::$javascript);
    self::setVar("ExternalJavaScript", self::$externaljavascript);
    self::setVar("CSS", self::$css);
    self::setVar("ExternalCSS", self::$externalcss);
    if(!self::$stop) {
      self::setVar("showSponsored",true);
      Callback::init("Smarty_display_load_vars");
      Callback::init("Smarty_display_js_api");
      Callback::init("Smarty_display_head_first");
      Callback::init("Smarty_display_head_last");
      Callback::init("Smarty_display_body_head");
      Callback::init("Smarty_display_body_main");
      Callback::init("Smarty_display_body_footer");
      Callback::init("Smarty_display_footer");
    }
  }

  public static function display($template) {
    self::$smarty->display($template);
  }

  public static function setVar($var,$value) {
    return self::$smarty->assign($var,$value,true);
  }

  public static function getVar($var) {
    return self::$smarty->getTemplateVars($var);
  }

  public static function addJS() {
    $numargs = func_get_args();
    foreach($numargs as $path) {
      if(array_search($path,self::$javascript)===false && file_exists(SYSTEM_PATH.$path)) {
        self::$javascript[] = $path;
      }
    }
  }

  public static function addExternalJS() {
    $numargs = func_get_args();
    foreach($numargs as $path) {
      if(array_search($path,self::$javascript)===false) {
        self::$externaljavascript[] = $path;
      }
    }
  }

  public static function addCSS() {
    $numargs = func_get_args();
    foreach($numargs as $path) {
      if(array_search($path,self::$css)===false && file_exists(SYSTEM_PATH."stylesheets/".$path)) {
        self::$css[] = $path;
      }
    }
  }

  public static function addExternalCSS() {
    $numargs = func_get_args();
    foreach($numargs as $path) {
      if(array_search($path,self::$css)===false) {
        self::$externalcss[] = $path;
      }
    }
  }

  public static function addHeaderTpl($file) {
    if(self::tpl_exists($file)) {
      self::setVar("addToHeader",$file);
    }
    else return false;
  }

  public static function setTempInRow($temp, $row="Smarty_display_body_main") {
    if(file_exists(self::CONTENT_TEMP_PATH."/".$temp)) {
      Callback::add_function($row, "::display", $temp);
    }
    else {
      throw new SysExc("Template nicht gefunden",401);
    }
  }

  public static function removeTmp($temp) {
    if(self::tpl_exists($temp)) {
      $k = array_search($temp, self::$templates);
      if($k !== false) {
        unset(self::$templates[$k]);
        return;
      }
    }
    SysExc::log(3, "Das Template konnte nicht entfernt werden.");
  }

  public static function tpl_exists($file) {
    return file_exists(self::CONTENT_TEMP_PATH."/".$file);

  }
}
