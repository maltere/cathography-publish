<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

use entities\Image_Category_Link;
use entities\Image_Category;
use entities\Image;

class Userpage extends Module {

  const MODULE_NAME = "userpage";

  public static function __install() {

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("required_module", "account" );
    self::set_config("required_module", "image_m" );

    self::set_config("loadable", true );

  }

  public function __construct() {
    Callback::add_function("Frontend_page_main", "::user_page","user");
  }

  public static function user_page() {
    if(GV::get("page_slug") == "user" && !empty(GV::URIoffset(1))) {
      $u = Database::em()->findOneBy("entities\User",array("username"=>GV::URIoffset(1)));
      if($u == NULL) {
        redirect(URL_PATH);
      }
      else {
        Smarty_i::setVar("user",$u);
        Smarty_i::setTempInRow("user.tpl","Smarty_display_body_main");
        Image_m::feed(1,4,$u);
      }
    }
  }
}
