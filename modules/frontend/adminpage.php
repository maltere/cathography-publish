<?php

class Adminpage extends apgexmpl {

  protected $module = "frontend";

  public $title = "Einstellungen";
  public $slug = "frontend";

  public function __construct() {

    parent::__construct();

    Smarty_i::setTempInRow("admin.tpl","Smarty_display_admin_head");
    Smarty_i::setTempInRow("admin_footer.tpl","Smarty_display_admin_footer");

    //$page[""]

    $this->pre_function();

    $this->switchPage($page);
    $this->createSubMenu($page);

    Smarty_i::setTempInRow($this->subtemplate,"Smarty_display_admin_main");
  }

  protected function pre_function() {
  }

  protected function pIndex() {
    $this->subtitle = "Webseite Einstellungen";
    $this->subtemplate = "admin_frontend_std.tpl";

    if(GV::URIoffset(2) == "edit") {
      if(!empty(GV::get("welcome_message"))) {
        $ret = Db_text::setVar("index_welcome", GV::get("welcome_message"));
      }
      if(!empty(GV::get("edit_page_title"))) {
        $ret = Meta_daten::setVar("page_title", GV::get("edit_page_title"),"title");
      }
    }

    $vari["startpagetext"] = Db_text::getVar("index_welcome");
    $vari["page_title"] = Meta_daten::getVar("page_title")->getValue();

    Smarty_i::setVar("frontend_menu",$vari);
  }

}
