<?php
/*******************

  VSoftware/ module/controller.php

  version:  1.0.0
  called:   ./system/module.php

  init:     1.  __config() required
            2.  __construct() required
            3. const MODULE_NAME required

*******************/

require_once __DIR__."/config.php";

class Frontend extends Module {

  const MODULE_NAME = "frontend";

  public static function __install() {

  }

  public static function __config() {

    /* set Settings variable */
    self::set_config("force_load", true );
    self::set_config("required_module", "menu_container" );
    self::set_config("required_module", "meta_daten" );
    self::set_config("required_module", "smarty_i" );
    self::set_config("required_module", "db_text" );
    self::set_config("required_module", "image_m" );
    self::set_config("loadable", true );

  }

  public function __construct() {
    Callback::add_function("System_init", "::systemInitCallback");
    Callback::add_function("System_init", "::selectPage");
    Callback::add_function("Smarty_display_load_vars", "::setBodyClasses");
    Callback::add_function("Smarty_display_js_api", "::jsAPI");
    Callback::add_function("Smarty_display_js_api", "::bodyClassAPI");
    Callback::add_function("Frontend_page_main", "::startPage", "index");
    Callback::add_function("Frontend_page_main", "::imagefeedPage", "imagefeed");
    //Admin::initAdmin("frontend","Seiteneinstellung","Frontend");
    Admin::add_to_menu("frontend","frontend","Einstellungen");
  }

  public static function adminPage() {
    $ad = new AdminPage("Frontend","frontend","adminPageIndex","Accountverwaltung",true,"admin_frontend_std.tpl");

    return $ad;
  }

  public static function jsAPI() {
    if(strlen(GV::get("getJSAPIdata"))>0) {
      $tmp=Smarty_i::$externaljavascript;
      $system = Smarty_i::getVar("system");
      foreach(Smarty_i::$javascript as $js) {
        $tmp[] = rtrim(URL_PATH,"/")."/".$js;
      }
      Smarty_i::setVar("plaintextVar",$tmp);
      header("Etag:".md5(serialize($tmp)));
      $age = 300;
      header('Cache-Control:private,max-age='.$age);
      header('Expires:'.gmdate('D, d M Y H:i:s \G\M\T', time()+$age));
      Smarty_i::display("plaintext.tpl");
      exit;
    }
  }

  public static function bodyClassAPI() {
    if(strlen(GV::get("getBODYAPIdata"))>0) {
      $j = "";
      if(count(self::$bclasses)>0) $j = join(" ",self::$bclasses);
      Smarty_i::setVar("plaintextVar",$j);
      Smarty_i::display("plaintext.tpl");
      exit();
    }
  }

  public static function adminPageIndex() {
    if(GV::get("frontend") == "edit") {
      if(!empty(GV::get("welcome_message"))) {
        $ret = Db_text::setVar("index_welcome", GV::get("welcome_message"));
      }
      if(!empty(GV::get("edit_page_title"))) {
        $ret = Meta_daten::setVar("page_title", GV::get("edit_page_title"),"title");
      }
    }

    $vari["startpagetext"] = Db_text::getVar("index_welcome");
    $vari["page_title"] = Meta_daten::getVar("page_title");
    Smarty_i::setVar("frontend_menu",$vari);
  }

  public static function systemInitCallback() {
    Smarty_i::setTempInRow("header.tpl", "Smarty_display_head_first");
    Smarty_i::setTempInRow("body_head.tpl", "Smarty_display_body_head");
    Smarty_i::setTempInRow("footer.tpl", "Smarty_display_footer");
    //Smarty_i::setTempInRow("header.tpl", "Smarty_display_head_first");
    Smarty_i::addJS("js/mr.js");
    Smarty_i::addCSS("screen.css");
    Smarty_i::addExternalJS("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js");
    Smarty_i::addExternalCSS("https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
    //SysExc::log(3,GV::get());
    Smarty_i::setVar("img",URL_PATH."img/");
    Smarty_i::setVar("menu",Menu_Container::get_menus());
    Smarty_i::setVar("menu_items",Menu_Container::$menu_Vars);
    Smarty_i::setVar("page_title",Meta_daten::getVar("page_title")->getValue());
  }

  public static function selectPage() {
    if(empty(GV::get("page_slug"))) {
      GV::set("page_slug","index");
    }
    else if(Callback::count_callbacks("Frontend_page_main", GV::get("page_slug")) == 0) {
      GV::set("page_slug","index");
    }
    Callback::init("Frontend_page_main");
  }

  public static function startPage() {
    if(GV::get("page_slug") == "index") {
      self::feedpage(1,"featured");
    }
  }
  public static function imagefeedPage() {
    $page = (int)GV::get("p");

    $page = $page<1? 1: $page;
    if(GV::get("page_slug") == "imagefeed") {
      self::feedpage($page,GV::get("type"));
    }
  }
  private static function feedpage($pages,$feed) {
    Smarty_i::setVar("welcome_message",Db_text::getVar("index_welcome"));
    Smarty_i::setVar("logoontiphidden",true);
    Smarty_i::setTempInRow("index.tpl","Smarty_display_body_main");
    Image_m::feed($pages,4,$feed);
  }

  public static $bclasses;
  public static function addBodyClass() {
    $numargs = func_get_args();
    foreach($numargs as $class) {
      if(is_string($class)) {
        self::$bclasses[] = $class;
      }
    }
  }
  public static function setBodyClasses() {
    $j = null;
    if(count(self::$bclasses)>0) $j = join(" ",self::$bclasses);
    Smarty_i::setVar("body_classes",$j);
  }
}
