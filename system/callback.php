<?
/*******************

  VSoftware/ system/callback.php

  version:  1.0.0
  called:   ./system/init.php

  use:      1.  Callback::init( $name ) um einen Callback aufzurufen
            2.  Callback::add_function( $name, $functione_name ) um function zum aufrufen hinzuzufuegen

*******************/

class Callback {

  private static $callback = array();

  public static function init ( $callback_name ) {

    if ( array_key_exists( $callback_name, Callback::$callback ) ) {
      foreach ( Callback::$callback[ $callback_name ] as $function ) {

        // $function[0] entspricht Funktionsstring
        // $function[1] sind die params
        $classicStatic = explode( "::", $function[0] );
        $params = $function[1];

        if(  is_string($params)  ) {
          $params = array($params);
        }

        if (!empty($classicStatic[1])) {
          if(is_array($params))
            call_user_func_array(  array( $classicStatic[0], $classicStatic[1] ), $params  );
          else {
            call_user_func(  array( $classicStatic[0], $classicStatic[1] )  );
          }
        }
        else {
          if(is_array($params))
            call_user_func_array(  $classicStatic[0], $params  );
          else {
            call_user_func(  $classicStatic[0]  );
          }
        }

      }
    }

  }

  public static function count_callbacks($callback_name, $params=NULL) {
    if(empty($params))
      return count(Callback::$callback[ $callback_name ]);
    else {
      if(is_array(Callback::$callback[ $callback_name ])) {
        $i = 0;
        foreach (Callback::$callback[ $callback_name ] as $function) {
          if($function[1] == $params) $i++;
        }
        return $i;
      }
      else return 0;
    }
  }

  public static function add_function ( $callback_name, $function, $params=null, $first = false ) {

    //check ob zu dem callback schon was angelegt wurde
    if ( !array_key_exists( $callback_name, Callback::$callback ) ) {
      Callback::$callback[ $callback_name ] = array();
    }

    // Wenn keine klasse angegeben wurde nehme ursprungsklasse (Also die Klasse von der der Callback hinzugefuegt wurde)
    // Wenn nur funktion dann gibts kein ::
    $classicStatic = explode( "::", $function );
    if (  empty($classicStatic[0])  ) {
      $debug = debug_backtrace();
      $class = $debug[1]["class"];
      $function = $class.$function;
    }

    $save = array($function,$params);

    if ( is_bool($first) && $first ) {
      array_unshift ( Callback::$callback[ $callback_name ], $save );
    }
    else if(is_numeric($first)) {
      Callback::$callback[ $callback_name ][$first] = $save;
    }
    else {
      array_push ( Callback::$callback[ $callback_name ], $save );
    }
    //var_dump(self::$callback);

  }

}
