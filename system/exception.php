<?php
/*******************

  VSoftware/ system/exception.php

  version:  1.00.0
  called:   ./index.php

  use:      1.  Throw SysExc Exception
            2.  Output Error by using getErrorMessage(  Bool $show,  Bool $html  )

*******************/

class SysExc extends Exception {

  private static $init = false;
  private static $smarty = false;
  private static $queue = array();

  public static function init($smifpo=true) {
    if(  Module::loaded("smarty_i") && $smifpo ) {
      Smarty_i::setTempInRow("error.tpl", "Smarty_display_body_main");
      self::$smarty = true;
    }
    self::$init = true;
    foreach(self::$queue as $val) {
      $val[3]->getErrorMessage($val[0],$val[1], $val[2]);
    }
  }

  public function getErrorMessage(  $show = true,  $html=true, $queue=false  ) {

    $debug = debug_backtrace();

    if($queue !== false) {
      $debug = $queue;
    }

    if(!self::$init && $debug[1]["function"] == "log" && $debug[1]["class"]=="SysExc") {
      self::$queue[] = array($show,$html,$debug,$this);
    }
    else {
      $line = $this->line;
      $file = $this->file;
      if (($debug[0]["class"] == "SysExc") && !empty($debug[1])) {
        $line = $debug[1]["line"];
        $file = $debug[1]["file"];
      }

      /* $msg:  Die HTML-Vorformatierte Error Nachricht. */
      $msg = $this->code." ".$this->message;
      $msg_show = $msg;
      $msg .= "in <strong>".$file.":".$line."</strong><br>\n";
      $msg .= $this->message;
      if(DEBUG)
        $msg_show = $msg;

      /* $clear_msg:  $msg: Ohne HTML und Umbrueche */
      $clear_msg = preg_replace(  "#(<strong>|</strong>)#",  '"',  $msg  );
      $clear_msg = preg_replace(  '/(?:<|&lt;)\/?([a-zA-Z]+) *[^<\/]*?(?:>|&gt;)/',  '',  $clear_msg  );
      $clear_msg = preg_replace(  "#(\r|\n)#",  ': ',  $clear_msg  );

      /* Error Log in der Datei die in der config.php angegeben wurde. */
      $type = "Error";
      if(  defined("ERROR_LOG_FILE")  ) {
        if (substr($this->code,0,1)==2) {
          error_log(  log_format(  $clear_msg,  "Warning"  ),  3,  ERROR_LOG_FILE  );
          $type = "Warning";
        }
        else if (substr($this->code,0,1)==3) {
          error_log(  log_format(  $clear_msg,  "Notice"  ),  3,  ERROR_LOG_FILE  );
          $type = "Notice";
        }
        else {
          error_log(  log_format(  $clear_msg,  "Error"  ),  3,  ERROR_LOG_FILE  );
        }
      }
      else {
        error_log(  log_format(  $clear_msg,  "Error"  ));
      }

      /* Wenn $show = True: Error wird ausgegeben */
      // if(  $show  ) {
        //var_dump(Module::loaded("smarty_i") && self::$init);
        if(  Module::loaded("smarty_i") && self::$smarty ) {
          self::smarty_error($this->code,$type,$file,$line,$this->message);
        }
        else {
          if(  $html  )
            echo "<p>$type ".$msg."</p>";
          else
            echo $clear_msg;
        }

      // }
    }
  } // getErrorMessage()

  private static function message($msg) {
    if(is_array($msg) || is_object($msg)) {
      return '<span class="printr">'.print_r($msg,true).'</span>';
    }
    return $msg;
  }

  public static function smarty_error($code,$type,$file,$line,$message) {
    if(  Module::loaded("smarty_i")  ) {
      $tmps = Smarty_i::getVar("errors");

      if(!is_array($tmps) || $tmps == NULL)
        $tmps = array();

      $tmp["code"] = $code;
      $tmp["type"] = $type;
      $tmp["file"] = $file;
      $tmp["line"] = $line;
      $tmp["message"] = $message;

      $tmps[] = $tmp;

      Smarty_i::setVar("errors", $tmps);
      if($debug[0]["class"] == "SysExc") Smarty_i::display("error.tpl");
      //var_dump($debug[0]["class"]);
    }
  }

    public static function log ($code,$owntext="", $show=EXCEPTION_SHOW) {
    $e = new SysExc();
    try {
      $owntext = self::message($owntext);
      switch ($code) {
        case 108:
          throw new SysExc ("Beim initalisieren der Tabelle ist was schief gelaufen.", 3);
        case 2:
          throw new SysExc ($owntext, 2);
        case 3:
          throw new SysExc ($owntext, 3);
        default:
          throw new SysExc ($owntext, (int)$code);
        break;
      }
    }
    catch (SysExc $e) {

      $e->getErrorMessage($show);

    }
  }

}
