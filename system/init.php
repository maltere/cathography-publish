<?php
/*******************

  VSoftware/ system/init.php

  version:  1.0.0
  called:   ./index.php

*******************/

require_once SYSTEM_PATH."system/functions/index.php";
require_once SYSTEM_PATH."system/globalvar.php";
require_once SYSTEM_PATH."system/callback.php";
require_once SYSTEM_PATH."system/doctrine.php";
require_once SYSTEM_PATH."system/module.php";

use entities\Group;

if(!file_exists(SYSTEM_PATH."system/functions/index.php")) throw new SysExc("Funktionen wurden nicht geladen",666);

class System {

  public static $initiated = false;
  public static $group = 1;
  public static $doctrine;

  public static function init() {

    GV::init();
    //var_dump(GV::$v);

    Database::init();

    if(GV::get("install")==="yes" ) {
        self::installRoutine();
    }
    self::$group = Database::em()->findOneBy("Group",array("slug"=>"default"));

    Module::load_module_list();

    $modules = Module::get_group_modules(self::$group);
    Module::load_module( $modules );

    SysExc::init();

    Callback::init("System_init");

    /* Ende der Funktion: System sollte jetzt initialisiert sein.
    ** Setze System Variable $initated auf true */
    self::$initiated = true;
    return self::$initiated;
  }

  protected static function installRoutine() {
    Database::em()->updateSchema("Group");
    Database::em()->updateSchema("Group_Module");
    Database::em()->updateSchema("Group_Setting");

    $count = Database::em()->simpleCount("Group");
    if($count<1) {
      $group = new Group("Standard","default");
      Database::em()->persist($group);
    }

    Database::em()->flush();


    file_put_contents(SYSTEM_PATH.".htaccess","
      <IfModule mod_rewrite.c>
      RewriteEngine On
      RewriteBase /".HTDOCS_URI."
      RewriteRule ^index\.php$ - [L]
      RewriteCond %{REQUEST_FILENAME} !-f
      RewriteCond %{REQUEST_FILENAME} !-d
      RewriteRule . index.php [L]
      </IfModule>
    ");
    file_put_contents(SYSTEM_PATH."nginx.conf",'
      # nginx configuration
      set $vspath '.HTDOCS_URI.';

      location /'.HTDOCS_URI.'system/ {
        deny all;
      }
      location /'.HTDOCS_URI.'modules/ {
        deny all;
      }
      location /'.HTDOCS_URI.'entities/ {
        deny all;
      }
      location /'.HTDOCS_URI.'doctrine/ {
        deny all;
      }
      location /'.HTDOCS_URI.'lib/ {
        deny all;
      }
      location /'.HTDOCS_URI.'log/ {
        deny all;
      }
    ');

    SysExc::log(3,"Die Tabellen wurden installiert.");



  }

}

?>
