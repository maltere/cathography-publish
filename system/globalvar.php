<?php

class GV {

  protected $v = array();
  public function __construct($offset=0) {
    $this->set_uri();
    $this->parse_get();
    $this->parse_post();
    $this->parse_URI($offset);
  }

  protected function parse_get() {
    foreach($_GET as $key=>$val) {
      if(isJson($val))
        $val = json_decode($val);

      $this->setV($key,$val,"get");
    }
  }

  protected function parse_post() {
    foreach($_POST as $key=>$val) {

      $this->setV($key,$val,"post");
    }
  }

  protected $uriexploded;
  protected function set_uri() {
      $URI_RE = preg_replace('/^' . preg_quote("/".ltrim(HTDOCS_URI,"/"), '/') . '/', '', $_SERVER["REQUEST_URI"]);
      $uri = explode("?",$URI_RE);
      $uri = explode("/",$uri[0]);
      $this->uriexploded = $uri;
  }
  protected function parse_URI($offset) {
    $uri = $this->uriexploded;

    $this->setV("page_slug",$uri[0+$offset]);
    for ($i=(1+$offset);$i<count($uri);$i++) {
      if(!empty($uri[$i])) {
        if(empty($uri[$i+1]))
          $val = true;
        else {
          $val = $uri[$i+1];
        }

        $this->setV($uri[$i],$val);
      }
      $i++;
    }
  }

  public function setV($key,$set,$katogorie=0) {
    $set = globalvarfilter($set);
    $this->v[globalvarfilter($key)] = $set;
    $this->v[strtoupper($katogorie)][globalvarfilter($key)] = $set;
  }

  public function getV($var=NULL,$k=false) {
    if (empty($var))
      return $this->v;
    else if($k===false)
      return $this->v[$var];
    else
      return $this->v[strtoupper($k)][$var];
  }

  public function getUri() {
    return $this->uriexploded;
  }

  protected static $globstd;

  public static function init() {
    self::$globstd = new GV();
  }

  public static function get($var=NULL,$k=false) {
    return self::$globstd->getV($var,$k);
  }

  public static function set($key,$set,$katogorie=0) {
    return self::$globstd->setV($key,$set,$katogorie);
  }

  public static function URIoffset($i) {
    $uri = self::$globstd->getUri();
    return $uri[$i];
  }
}
