<?php

use Doctrine\Common\ClassLoader;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once SYSTEM_PATH."doctrine/vendor/autoload.php";

$loader = array();
$loader[0] = new \Doctrine\Common\ClassLoader("Doctrine");
$loader[0]->register();
$loader[1] = new ClassLoader("entities", SYSTEM_PATH);
$loader[1]->register();

class Database {

  protected static $sem = null;
  protected $em = null;
  protected $evm = null;

  function __construct($dbname, $host, $user, $password, $prefix="", $dbsystem="MYSQL", $charset="utf8") {

    $this->evm = new \Doctrine\Common\EventManager;

    $path = array(SYSTEM_PATH."system/entities/",SYSTEM_PATH."entities/");

    if(EXCEPTION_SHOW)
      $dev_mode = true;
    else $dev_mode = false;

    $this->tablePrefix($prefix);

    if($dbsystem == "PGSQL") $dbsystem = "pdo_pgsql";
    else $dbsystem = "pdo_mysql";

    $dbParams = array(
        'driver' => $dbsystem,
        'user' => $user,
        'password' => $password,
        'host' => $host,
        'dbname' => $dbname,
        'charset' => $charset,
        'driverOptions' => array('charset'=>$charset)
    );

    $config = Setup::createAnnotationMetadataConfiguration($path, $dev_mode);

    $this->em = EntityManager::create($dbParams, $config, $this->evm);
    $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    $this->getFullNamespace("Group");
  }

  protected function getEntityNames() {
    $classes = array();
    $metas = $this->em->getMetadataFactory()->getAllMetadata();
    foreach ($metas as $meta) {
      $classes[] = $meta->getName();
    }
    return $classes;
  }

  public function getFullNamespace($entity) {
    foreach($this->getEntityNames() as $class) {
      $sub = explode('\\',$class);
      if(end($sub)===$entity) return $class;
    }
    return $entity;
  }

  protected function tablePrefix($prefix) {

    require_once SYSTEM_PATH."lib/doctrine_table_prefix.php";

    $tablePrefix = new \DoctrineExtensions\TablePrefix($prefix);
    $this->evm->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, $tablePrefix);
  }

  public function updateSchema($entity) {

    $namespacepath ="entities\\".$entity;

    if(class_exists($namespacepath) && DEBUG) {
      $tool = new \Doctrine\ORM\Tools\SchemaTool($this->em);

      $classes = array(
        $this->em->getClassMetadata($namespacepath)
      );
      $tool->updateSchema($classes,true);
    }
  }

  public function persist($entity) {
    $this->em->persist($entity);
  }

  public function remove($entity) {
    $this->em->remove($entity);
  }

  public function find($entityName,$id) {
    $entityName = $this->getFullNamespace($entityName);
    return $this->em->find($entityName,$id);
  }

  public function findBy($entityName,$array,$order=null,$limit=null,$offset=null) {
    $entityName = $this->getFullNamespace($entityName);
    return $this->em->getRepository($entityName)->findBy($array,$order,$limit,$offset);
  }

  public function findOneBy($entityName,$array,$order=null,$limit=null,$offset=null) {
    $entityName = $this->getFullNamespace($entityName);
    return $this->em->getRepository($entityName)->findOneBy($array,$order,$limit,$offset);
  }

  protected $flushcount=0;
  public function flush() {
      $this->em->flush();
  }

  public function simpleCount($entityName, $field="id") {
    $entityName = $this->getFullNamespace($entityName);

    $qb = $this->qb();
    $qb->select('COUNT(g.'.$field.')')
       ->from($entityName,"g");

    $r = $qb->getQuery()->getOneOrNullResult();

    return $r[1];
  }

  public function qb() {
    $qb = $this->em->createQueryBuilder();
    return $qb;
  }

  public static function init() {
    self::$sem = new Database(DB_NAME,DB_HOST,DB_USER,DB_PASSWORD,DB_TB_PRE,DB_SYSTEM);
  }

  public static function &em($d = false) {
    // if(!self::$sem->em->isOpen()) {
    //   self::$sem->em = self::$sem->em->create(
    //       self::$sem->em->getConnection(),
    //       self::$sem->em->getConfiguration()
    //   );
    // }
    if($d) return self::$sem->em;
    return self::$sem;
  }

}
