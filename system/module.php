<?php

/*******************

  VSoftware/ system/module.php

  version:  1.1.0
  called:   ./system/init.php

  init non-static:      1.  __config() required
                        2.  __construct() required
                        3. const MODULE_NAME required

  init static:          1. load Module::load_module_list()
                        2. load Module::get_group_modules() [optional]
                        3. load Module::load_module(  Array of Modules  )

*******************/
require_once SYSTEM_PATH."system/exception.php";

class Module {

  const PATH = SYSTEM_PATH."modules/";

  public static $list;
  public static $settings;
  public static $loaded_module;

  /* bekommt ein Array mit Modulen uebergeben (Siehe format get_group_modules()) und laedt im Anschluss die Module in $loaded_module */
  public static function load_module (  $array  ) {

    if ( is_array($array) ) {
      foreach ( $array as $module ) {

        if ( self::module_exists($module["module"]) ) {

          $className = titleCase($module["module"]);
          require_once $module["path"]."controller.php";
          self::$loaded_module[$module["module"]]["obj"] = new $className();
          self::$loaded_module[$module["module"]]["path"] = $module["path"];

          //self::$loaded_module[$module["module"]]->init();

        }

      }
    }
    else {
      throw new SysExc ( "Array aus Modulen erwartet. Was anderes bekommen.", 201);
    }

  }

  /* Ladet eine Liste der Module die existieren.
  ** Speicherort der Module: ./system/modules
  */
  public static function load_module_list () {
    self::$list = array();
    $modules_sub_dir = glob(  self::PATH."*"  ,  GLOB_ONLYDIR  );

    foreach(  $modules_sub_dir as $value  ) {
      $tmp = null;

      $value = $value . "/";

      $tmp["module"] = basename($value);
      $tmp["path"]  = $value;

      $modul_settings_path = $value."controller.php";


      if (  file_exists($modul_settings_path)  ) {
        require_once $modul_settings_path;

        /* Lade Config Infos aus dem Module */
        if(GV::get("install")==="yes")
          call_user_func(  array(  titleCase( $tmp["module"]  ), "__install" )  );
        call_user_func(  array(  titleCase( $tmp["module"]  ), "__config" )  );

        $tmp["force_load"]  = self::$settings[$tmp["module"]]["force_load"];

        array_push(  self::$list,  $tmp  );
      }

    }

  }

  /* Prueft ob ein Modul existiert und ob es in den Einstellungen als ladbar markiert wurde.
  ** Vorraussetzung dafür ist das load_module_list() aufgerufen wurde. (Normalerweise in init.php). */
  public static function module_exists ($name) {

    if (  file_exists(self::PATH.$name) && self::$settings[$name]["loadable"] && file_exists(self::PATH.$name."/controller.php") ) {
      return true;
    }
    return false;

  }

  public static function loaded($module) {
    return isset(self::$loaded_module[$module]);
  }

  /* Übergibt eine Group ID.
  ** Als nöchstes wird in der Datenbank geprueft welche Module geladen werden sollen.
  ** Module die zusaetzlich immer geladen werden sollen werden auch noch hinzugefuegt.
  ** Zurueckgegeben wird eine Liste aller zu ladenendenn Module in form eines Arrays. */
  public static function get_group_modules ($group) {

    /* Frage Module fuer group aus der Datenbank an */
    $data = $group->getModules();
    $added  = array();
    $return = array();

    if ($data !== NULL) {


      foreach($data as $row) {

        $tmp    = array();

        if (  self::module_exists(  $row->getName()  )  ) {

          $tmp = self::get_all_required_modules($row->getName(), $added);

          $added = array_merge(  $added,  self::added_array($tmp)  );
          $return = array_merge(  $return,  $tmp  );

        }

      }

    }

    /* Fuege die Pflichtmodule mit dem attribute forced hinzu */
    foreach (  self::$list as $value) {

      $tmp    = array();

      if ( !array_key_exists($value["module"], $added) && $value["force_load"] === true ) {

        $tmp = self::get_all_required_modules($value["module"], $added);

        $added = array_merge(  $added,  self::added_array($tmp)  );
        $return = array_merge(  $return,  $tmp  );

      }

    }

    return $return;

  }
  private static function added_array ( $module ) {

    $array = array();

    foreach ($module as $key => $value) {
      $array[$value["module"]] = true;
    }

    return $array;

  }

  /* Liefert das übergegebene und alle benoetigten Module als Array zurueck */
  private static function get_all_required_modules ( $module, $added ) {

    $array = array();

    if ( self::module_exists ($module) ) {
      if( !$added[$module] ) {

        $tmp["module"] = $module;
        $tmp["path"]  = self::PATH . $module."/";
        $added[$module] = true;

        /* lade rekursiv die required_module Liste */
        if ( !empty(self::$settings[$module]["required_module"][0]) ) {
          foreach (  self::$settings[$module]["required_module"] as $required  ) {
            $array = self::get_all_required_modules($required, $added);
          }
        }

        /* Fuege die required_module Liste mit dem Modul zusammen. Requirements zuerst. */
        array_push ($array, $tmp);
      }

      return $array;
    }
    else {
      throw new SysExc ("Das Module <strong>".$module."</strong> konnte nicht gefunden werden", 202);
    }

  }

  /* non static part */

  protected static function set_config( $key, $var ) {

    $debug = debug_backtrace();
    $module = strtolower($debug[1]["class"]);

    if(file_exists(self::PATH.$module)) {

      if ( $key == "required_module" ) {

        /* Alle benoetigten Arrays werden zusammengefasst */

        self::$settings[$module]["required_module"][] = $var;

      }
      else {
        self::$settings[$module][$key] = $var;
      }

    }

  }

}


?>
