<?php
namespace entities;
use entities\Group_Module;
use entities\Group_Setting;
use Database;

/** @Entity
 ** @Table(name="group") **/
class Group {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @Column(type="string", length=30, nullable=false) **/
  protected $name;
  /** @Column(type="string", length=30, nullable=false) **/
  protected $slug;
  /** @OneToMany(targetEntity="Group_Module", mappedBy="group",
        cascade={"persist","remove"}) **/
  protected $modules;
  /** @OneToMany(targetEntity="Group_Setting", mappedBy="group",
        cascade={"persist","remove"}) **/
  protected $settings;

  public function __construct($name,$slug) {
    $this->name = $name;
    $this->slug = $slug;
  }

  public function getID() {
    return $this->id;
  }

  public function setName($value) {
    $this->name = $value;
  }
  public function getName() {
    return $this->name;
  }

  public function setSlug($value) {
    $this->slug = $value;
  }
  public function getSlug() {
    return $this->slug;
  }

  public function addModule($value) {
    $this->modules[] = new Group_Module($this,$value);
  }
  public function getModules() {
    return $this->modules;
  }

  public function setSetting($var,$value) {
    $h = $this->getSetting($var);
    if(empty($h)) {
      $this->settings[] = new Group_Setting($this,$var,$value);
    }
    else {
      $h->setValue($value);
    }
  }
  public function getSettings() {
    return $this->settings;
  }
  public function getSetting($var) {
    $field=array("group"=>$this,"var"=>$var);
    $re = Database::em()->findOneBy("Group_Setting",$field);
    return $re;
  }


}

 ?>
