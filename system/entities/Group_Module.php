<?php
namespace entities;

/** @Entity
 ** @Table(name="group_module") **/
class Group_module {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group", inversedBy="modules")
  **  @JoinColumn(nullable=false)*/
  protected $group;
  /** @Column(type="string", length=30, nullable=false) **/
  protected $module_name;

  public function __construct(Group $group, $name) {
    $this->setGroup($group);
    $this->setName($name);
  }

  public function setGroup(Group $value) {
    $this->group = $value;
  }
  public function getGroup() {
    return $this->group;
  }

  public function setName($value) {
    $this->module_name = $value;
  }
  public function getName() {
    return $this->module_name;
  }


}

 ?>
