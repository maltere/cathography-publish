<?php
namespace entities;

/** @Entity
 ** @Table(name="group_settings") **/
class Group_Setting {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group", inversedBy="settings")
  **  @JoinColumn(nullable=false)*/
  protected $group;
  /** @Column(type="string", length=20, nullable=false) **/
  protected $var;
  /** @Column(type="string", length=80, nullable=false) **/
  protected $value;
  /** @Column(type="string", length=80, nullable=true) **/
  protected $module;
  /** @Column(type="string", length=80, nullable=true) **/
  protected $meta;

  public function __construct(Group $group,$var,$value) {
    $this->setGroup($group);
    $this->setVar($var);
    $this->setValue($value);
  }

  public function getID() {
    return $this->id;
  }

  public function setGroup(Group $value) {
    $this->group = $value;
  }
  public function getGroup() {
    return $this->group;
  }

  public function setVar($value) {
    $this->var = $value;
  }
  public function getVar() {
    return $this->var;
  }

  public function setValue($value) {
    $this->value = $value;
  }
  public function getValue() {
    return $this->value;
  }

  public function setModule($value) {
    $this->module = $value;
  }
  public function getModule() {
    return $this->module;
  }

  public function setMeta($value) {
    $this->meta = $value;
  }
  public function getMeta() {
    return $this->meta;
  }


}

 ?>
