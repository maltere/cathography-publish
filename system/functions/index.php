<?php
/*******************

  VSoftware/ system/functions/index.php

  version:  1.00.0
  called:   ./system/init.php

  Scannt das dir nach dateien und lädt die Dateien.

*******************/

/* Lade die DIR-Liste */
$dir = scandir(__DIR__);

/* Filtern des Arrays */
$dir = array_filter($dir, "php_function");
$dir = array_diff($dir,   array("index.php"));

function php_function($var) {

  return (substr($var,-9) === ".func.php");

}

/* Alle gefundenen Dateien laden */
foreach ($dir as $function) {

  require_once __DIR__ ."/". $function;

}

 ?>
