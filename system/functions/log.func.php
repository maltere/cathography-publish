<?php

/*******************

  Funktionsname: log_format($msg,$type);

*******************/

function log_format($msg, $type="Undefined") {

  return "[".date("Y-m-d H:i:s",time())."][".$type."] ".$msg."\n";

}
