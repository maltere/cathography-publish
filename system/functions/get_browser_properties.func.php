<?php

function get_browser_properties(){

$browser =array();

$agent=$_SERVER['HTTP_USER_AGENT'];

if(stripos($agent,"firefox")!==false){

$browser['browser'] = 'Firefox'; // Set Browser Name

$domain = stristr($agent, 'Firefox');

$split =explode('/',$domain);

$browser['version'] = $split[1]; // Set Browser Version

}

if(stripos($agent,"Opera")!==false){

$browser['browser'] = 'Opera'; // Set Browser Name

$domain = stristr($agent, 'Version');

$split =explode('/',$domain);

$browser['version'] = $split[1]; // Set Browser Version

}

if(stripos($agent,"MSIE")!==false){

$browser['browser'] = 'Internet Explorer'; // Set Browser Name

$domain = stristr($agent, 'MSIE');

$split =explode(' ',$domain);

$browser['version'] = $split[1]; // Set Browser Version

}

if(stripos($agent,"Chrome")!==false){

$browser['browser'] = 'Google Chrome'; // Set Browser Name

$domain = stristr($agent, 'Chrome');

$split1 =explode('/',$domain);

$split =explode(' ',$split1[1]);

$browser['version'] = $split[0]; // Set Browser Version

}

else if(stripos($agent,"Safari")!==false){

$browser['browser'] = 'Safari'; // Set Browser Name

$domain = stristr($agent, 'Version');

$split1 =explode('/',$domain);

$split =explode(' ',$split1[1]);

$browser['version'] = $split[0]; // Set Browser Version

}

return $browser;

}
