<?php

function unlink_mul(...$par) {
  $ret = true;
  foreach($par as $file ) {
    if(file_exists($file)) {
       $ret = $ret && unlink($file);
    }
  }

  return $ret;
}

 ?>
