<?

function alphanumeric($input,$less=false) {
  if($less===true)
    return preg_replace("/[^a-zA-Z0-9_-]/", "", $input);
  if($less===1)
    return preg_replace("/[^a-zA-ZßäöüÄÖÜ0-9&§@.%(!?)_-]/", "", $input);
  else
    return preg_replace("/[^a-zA-ZßäöüÄÖÜ0-9&§%(!?)*_-]/", "", $input);
}

function alphanumeric_r($input) {
  if(is_array($input)) {
    foreach($input as $key=>$value) {
      $key = alphanumeric($key);
      $value = alphanumeric_r($value);
      $tmp[$key] = $value;
    }
  }
  else {
    $tmp = alphanumeric($input);
  }
  return $tmp;
}
