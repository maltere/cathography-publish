<?php

/* Set DIR_Path */
define("HTDOCS_PATH", $_SERVER["DOCUMENT_ROOT"]."/");

/* Set DIR_Path */
define("HTDOCS_URI", "cathography/");

/* Set URL_Path */
define("HTTP_OR_HTTPS",isset($_SERVER["HTTPS"]) ? 'https' : 'http');
define("URL_PATH", HTTP_OR_HTTPS."://".$_SERVER["HTTP_HOST"]."/".HTDOCS_URI);
define("URL_FULL_PATH", HTTP_OR_HTTPS."://".$_SERVER['HTTP_HOST'] . explode("?",$_SERVER['REQUEST_URI'])[0]);

/* Set DIR_Path */
define("SYSTEM_PATH", HTDOCS_PATH.HTDOCS_URI);

define("DEBUG",true);

/* Exception anzeigen: True or False */
define("EXCEPTION_SHOW", DEBUG);

/* Set Error_Log File */
define("ERROR_LOG_FILE", SYSTEM_PATH."log/sys-error.log");

/* Datenbank System */
define("DB_SYSTEM", "MYSQL");

/* Datenbank Host */
define("DB_HOST","localhost");

/* Datenbank User */
define("DB_USER","root");

/* Datenbank Password */
define("DB_PASSWORD","root");

/* Datenbank Name */
define("DB_NAME","cathography");

/* Datenbank Tabellen Prefix */
define("DB_TB_PRE","cp2_");

/* System Email */
define("SYSTEM_EMAIL","black@malte-reddig.de");

/* Fehlerlevel */
ini_set("display_errors", 1);
error_reporting(E_ALL & ~E_NOTICE);
