<?php
namespace entities;
use entities\Group;
use entities\User;
use Database;
use System;

/** @Entity
 ** @Table(name="image")
 ** @HasLifecycleCallbacks **/
class Image {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $group;
  /** @ManyToOne(targetEntity="User")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $user;
  /** @OneToMany(targetEntity="Image_Category_Link", mappedBy="image",
        cascade={"persist","remove"})
  **  @JoinColumn(nullable=false, referencedColumnName="id") **/
  protected $categories;
  /** @Column(type="string", length=60, nullable=false) **/
  protected $name;
  /** @Column(type="string", length=60, nullable=false) **/
  protected $slug;
  /** @Column(type="string", length=60, nullable=false) **/
  protected $path;
  /** @Column(type="string", length=20, nullable=false) **/
  protected $format;
  /** @Column(type="text", nullable=true) **/
  protected $meta;
  /** @Column(type="datetime") */
  protected $upload;
  /** @Column(type="datetime") */
  protected $modified;

  public function __construct(Group $group, User $user,$name,$path,$format) {
    $this->group = $group;
    $this->setUser($user);
    $this->setName($name);
    $this->setPath($path);
    $this->setSlug( md5($this->getName()) );
    $this->setFormat($format);
    $this->upload = new \DateTime();
  }

  /**
   * @PrePersist
   * @PreUpdate
   */
  public function onPrePersistUpdate()
  {
      $this->modified = new \DateTime();
  }

  protected $somenumber;
  public function getID($r=false) {
    if($r) {
      if($this->somenumber<1) {
        mt_srand((int)(microtime(true)*$this->id*1000));
        $this->somenumber = (mt_rand(1,200000));
      }
      return $this->somenumber;
    }
    return $this->id;
  }

  public function getUpload() {
    return $this->upload;
  }

  public function setUser(User $value) {
    $this->user = $value;
  }
  public function getUser() {
    return $this->user;
  }

  public function setName($value) {
    $this->name = $value;
  }
  public function getName() {
    return $this->name;
  }

  public function setPath($value) {
    $this->path = $value;
  }
  public function getPathDB() {
    return $this->path;
  }
  public static $upload_path = "upload/images/";
  public function getPath($size="normal", $absolute=true) {
    $upload_path = self::$upload_path; // Hier liegen die Originale
    $image_sizes = "img/upload/"; // Der Pfad wo die Groesen Liegen!

    $return_path = "";
    if($absolute) {
      $return_path = SYSTEM_PATH;
    }

    switch (strtolower($size)) {
      case "original":
        $switch = $upload_path; break;
      case "large":
        $switch = $image_sizes."large/"; break;
      case "normal":
        $switch = $image_sizes."normal/"; break;
      case "small":
        $switch = $image_sizes."small/"; break;
      case "medium":
        $switch = $image_sizes."medium/"; break;
      case "fullsz":
        $switch = $image_sizes."fullsz/"; break;
      default:
        $switch = $image_sizes."normal/";
    }

    $return_path .= $switch.$this->getPathDB();

    return $return_path;


  }
  public function getURL($size="viewer", $absolute=true) {
    $upload_path = "upload/images/"; // Hier liegen die Originale
    $image_sizes = "img/upload/"; // Der Pfad wo die Groesen Liegen!

    $return_path = "";
    if($absolute) {
      $return_path = URL_PATH;
    }

    $return_path .= "image/slug/".$this->getSlug();

    switch (strtolower($size)) {
      case "large":
        $switch = "out/".$this->getHash("size","large"); break;
      case "normal":
        $switch = "out/".$this->getHash("size","normal"); break;
      case "small":
        $switch = "out/".$this->getHash("size","small"); break;
      case "medium":
        $switch = "out/".$this->getHash("size","medium"); break;
      case "fullsz":
        $switch = "out/".$this->getHash("size","fullsz"); break;
      case "delete":
        $return_path = "";
        if($absolute) {
          $return_path = URL_PATH;
        }
        $return_path .= "profile/image/list/id/".$this->getID();
        $switch = "delete/".$this->getHash("delete"); break;
      default:
        $switch = "";
    }

    return $return_path."/".$switch;

  }

  public function setSlug($value) {
    $this->slug = $value;
  }
  public function getSlug() {
    return $this->slug;
  }

  public function setMeta($value) {
    $this->meta = json_encode($meta);
  }
  public function getMeta() {
    return json_decode($this->meta,true);
  }

  public function setFormat($value) {
    $this->format = $value;
  }
  public function getFormat() {
    return $this->format;
  }

  public function getModified($format=null) {
    if($format != null) {
      return $this->modified->format($format);
    }
    return $this->modified;
  }

  public function getUploadDate($format=null) {
    if($format != null) {
      return $this->upload->format($format);
    }
    return $this->upload;
  }

  public function addCategory($var) {
    $h = $this->getCategory($var);
    if(empty($h)) {
      $this->categories[] = new Image_Category_Link(System::$group,$this,$var);
    }
  }
  public function getCategories() {
    return $this->categories;
  }
  public function getCategory($var) {
    if(!is_numeric($var)) {
      $var = Database::em()->findOneBy("Image_Category",array("slug"=>$var));
    }
    $field=array("group"=>System::$group,"category"=>$var,"image"=>$this);
    $re = Database::em()->findOneBy("Image_Category_Link",$field);
    return $re;
  }

  private $salt = '@p2p4|EZt?jo"u\<8,GoqAWmv9oQ*HzMf?[BR4I2=??!m#62LBRE?6C';
  public function getHash($reason = "file",$salt=NULL) {
    if($reason == "delete") {
      return @md5( $this->getID() . "deleteit" . $this->getPathDB() );
    }
    if($reason == "size") {
      return @md5( $this->getID() . $salt.$this->salt . $this->getSlug() );
    }
    else {
      return @hash_file("haval160,4",$this->getPath("normal"));
    }
  }

  protected $size;
  protected $resolution;
  public function getResolution() {
    if($this->resolution == NULL) {
      $this->size = @getImageSize($this->getPath("normal"));
      $this->resolution = @number_format(($this->size[1]/$this->size[0])*100,2);
    }
    return $this->resolution;
  }

  protected $color = null;
  protected function getColor() {

    if($this->color == null) {

      $filename = $this->getPath("medium");
      $img = imagecreatefromjpeg($filename);
      $imgSize = getImageSize($filename);
      $imgWidth = $imgSize[0];
      $imgHeight = $imgSize[1];
      $jump[0] = (int)floor(10*($imgWidth/420));
      $jump[1] = (int)floor(10*($imgHeight/420));
      $mod[0] = (int)ceil(25*($imgWidth/420));
      $mod[1] = (int)ceil(25*($imgHeight/420));
      //var_dump($jump,$mod);

      //Farben zählen
      $i=1;
      $red = 0;
      $blue = 0;
      $green = 0;
      if($img) {
        for($x = 0; $x < $imgWidth; $x++){
            for($y = 0; $y < $imgHeight; $y++){
                //$colors ergibt ein Array mit dem imagecolor als Index und dem Zähler als Wert
                $rgb =  imagecolorsforindex($img, imagecolorat($img, $x, $y));
                $red += $rgb['red'];
                $green += $rgb['green'];
                $blue += $rgb['blue'];
                $i++;

                if($y%$mod[1]==0) {
                  $y+=$jump[1];
                }
            }
            if($x%$mod[0]==0) {
              $x+=$jump[0];
            }
        }
        imagedestroy($img);
      }

      $this->color = array("red"=>(int)($red/$i), "green"=>(int)($green/$i), "blue"=>(int)($blue/$i));
    }
    return $this->color;
  }

  public function imgBrightness() {
    $color = @$this->getColor();
    $avcolor = (int)(array_sum($color)/3);

    return number_format(($avcolor/256), 2);
  }

  public function averageColor($type="hex",$times=1.0,$add=0) {
    $color = @$this->getColor();

    if($type=="hex") {
      $r = chexform(dechex((float)$times*$color["red"]+$add));
      $g = chexform(dechex((float)$times*$color["green"]+$add));
      $b = chexform(dechex((float)$times*$color["blue"]+$add));

      return strtoupper("#".$r.$g.$b);
    }
    else return $color;
  }

  public function uploaded() {
    $uploaded = Database::em()->findOneBy("Image_Category",array("slug"=>"uploaded"));
    $db = Database::em()->findOneBy("Image_Category_Link",array("image"=>$this,"category"=>$uploaded));
    return count($db)>0;
  }

  public function access($user) {
    if($this->uploaded()) {
      return true;
    }
    else {
      if($user != null) {
        return $user == $this->user;
      }
    }
    return false;
  }

  protected $tmpCategory;
  public function setTmpCategory($value) {
    $this->tmpCategory = $value;
  }

  public function getTmpCategory() {
    return $this->tmpCategory;
  }


}

function chexform($r) {
  if(strlen($r)==1){
    $r = "0".$r;
  }
  else if(strlen($r)<1){
    $r = "00";
  }
  else if(strlen($r)>2){
    $r = "FF";
  }
  return $r;
}

 ?>
