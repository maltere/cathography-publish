<?php
namespace entities;
use entities\Group;
use Database;

/** @Entity
 ** @Table(name="text")
 ** @HasLifecycleCallbacks **/
class DB_Text {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $group;
  /** @Column(type="string", length=30, nullable=false) **/
  protected $slug;
  /** @Column(type="text", nullable=false) **/
  protected $value;
  /** @Column(type="datetime") */
  protected $modified;

  public function __construct($group,$slug,$value) {
    $this->group = $group;
    $this->setSlug($slug);
    $this->setValue($value);
  }

  /**
   * @PrePersist
   * @PreUpdate
   */
  public function onPrePersistUpdate()
  {
     $this->modified = new \DateTime();
  }

  public function getID() {
    return $this->id;
  }

  public function setValue($value) {
    $this->value = $value;
  }
  public function getValue() {
    return $this->value;
  }

  public function setSlug($value) {
    $this->slug = $value;
  }
  public function getSlug() {
    return $this->slug;
  }

  public function getModified() {
    return $this->modified;
  }


}

 ?>
