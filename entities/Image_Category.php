<?php
namespace entities;

use entities\Group;
use Database;
use System;

/** @Entity
 ** @Table(name="image_category")
 ** @HasLifecycleCallbacks  **/
class Image_Category {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false)*/
  protected $group;
  /** @OneToMany(targetEntity="Image_Category_Link", mappedBy="category",
        cascade={"persist","remove"}) **/
  protected $images;
  /** @Column(type="string", length=40, nullable=false) **/
  protected $name;
  /** @Column(type="string", length=60, nullable=false) **/
  protected $slug;

  public function __construct(Group $group, $name, $slug) {
    $this->setGroup($group);
    $this->setName($name);
    $this->setSlug($slug);
  }

  public function getID() {
    return $this->id;
  }

  public function setGroup(Group $value) {
    $this->group = $value;
  }
  public function getGroup() {
    return $this->group;
  }

  public function setName($value) {
    $this->name = $value;
  }
  public function getName() {
    return $this->name;
  }

  public function setSlug($value) {
    $this->slug = $value;
  }
  public function getSlug() {
    return $this->slug;
  }

  public function addImage($var) {

    $last = Database::em()->findOneBy("Image_Category_Link",array("category"=>$this,"reviewed"=>0),array("position"=>DESC));
    $position = 30000;
    if($last != NULL)
      $position = (int)($last->getPosition())+1000;

    $h = $this->getImage($var);
    if(empty($h)) {
      $this->images[] = new Image_Category_Link(System::$group,$var,$this,$position);
    }
  }
  public function getImages() {
    return $this->images;
  }
  public function getImage($var) {
    $field=array("group"=>System::$group,"category"=>$this,"image"=>$var);
    $re = Database::em()->findOneBy("Image_Category_Link",$field);
    return $re;
  }

  public static function get($slug,$group) {
    if(is_numeric($slug)) {
      return Database::em()->find("Image_Category",$slug);
    }
    else {
      return Database::em()->findOneBy("Image_Category",array("slug"=>$slug,"group"=>$group));
    }
  }


}

 ?>
