<?php
namespace entities;

/** @Entity
 ** @Table(name="image_category_link")
 ** @HasLifecycleCallbacks **/
class Image_Category_Link {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false)*/
  protected $group;
  /** @ManyToOne(targetEntity="Image",inversedBy="categories")
  **  @JoinColumn(nullable=false)*/
  protected $image;
  /** @ManyToOne(targetEntity="Image_Category",inversedBy="images")
  **  @JoinColumn(nullable=false)*/
  protected $category;
  /** @Column(type="integer", length=2, nullable=false) **/
  protected $reviewed = 0;
  /** @Column(type="datetime") */
  protected $modified;
  /** @Column(type="integer",length=8,nullable=true) */
  protected $position;

  public function __construct(Group $group, Image $image, Image_Category $category,$position=0) {
    $this->setGroup($group);
    $this->image = $image;
    $this->category = $category;
    $this->setPosition($position);
  }

  /**
   * @PrePersist
   * @PreUpdate
   */
  public function onPrePersistUpdate()
  {
      $this->modified = new \DateTime();
  }
  public function getID() {
    return $this->id;
  }

  public function setGroup(Group $value) {
    $this->group = $value;
  }
  public function getGroup() {
    return $this->group;
  }

  public function setImage(Image $value) {
    $this->image = $value;
  }
  public function getImage() {
    return $this->image;
  }

  public function setCategory(Image_Category $value) {
    $this->category = $value;
  }
  public function getCategory() {
    return $this->category;
  }

  public function setPosition($value) {
    $this->position = $value;
  }
  public function getPosition() {
    return $this->position;
  }


}

 ?>
