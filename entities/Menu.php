<?php
namespace entities;
use entities\Group;
use entities\Menu_Item;
use Database;

/** @Entity
 ** @Table(name="menu")
 ** @HasLifecycleCallbacks **/
class Menu {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $group;
  /** @Column(type="string", length=20, nullable=false) **/
  protected $title;
  /** @Column(type="string", length=60, nullable=false) **/
  protected $slug;
  /** @Column(type="boolean", nullable=false) **/
  protected $active = True;
  /** @OneToMany(targetEntity="Menu_Item", mappedBy="menu",
        cascade={"persist","remove"})
  **  @JoinColumn(nullable=false, referencedColumnName="id") **/
  protected $items;

  public function __construct(Group $group, $title, $slug, $active = True) {
    $this->group = $group;
    $this->setTitle($title);
    $this->setSlug($slug);
    $this->setActive($active);
  }

  public function getID() {
    return $this->id;
  }

  public function setTitle($value) {
    $this->title = $value;
  }
  public function getTitle() {
    return $this->title;
  }

  public function setSlug($value) {
    $this->slug = $value;
  }
  public function getSlug() {
    return $this->slug;
  }

  public function setActive($value) {
    $this->active = (bool)$value;
  }
  public function getActive() {
    return $this->active;
  }

  public function addItem($title,$path,$css=NULL) {
    $this->items[] = new Menu_Item($this,$title,$path,$css);
  }
  public function getItems() {
    return $this->items;
  }

  public static function get($slug) {
    if(is_numeric($slug)) {
      return Database::em()->find("Menu",$slug);
    }
    else {
      return Database::em()->findOneBy("Menu",array("slug"=>$slug));
    }
  }


}

 ?>
