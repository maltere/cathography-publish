<?php
namespace entities;
use \Database;
use \Account;

/** @Entity
 ** @Table(name="user") **/
class User {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @Column(type="string", length=40, nullable=false, unique=true) **/
  protected $username;
  /** @Column(type="string", length=50, nullable=false, unique=true) **/
  protected $email;
  /** @Column(type="string", length=50, nullable=false) **/
  protected $firstname;
  /** @Column(type="string", length=50, nullable=false) **/
  protected $lastname;
  /** @Column(type="datetime", nullable=false) **/
  protected $register_date;
  /** @Column(type="string", length=140, nullable=true) **/
  protected $password;
  /** @Column(type="string", length=255, nullable=true) **/
  protected $salt;
  /** @Column(type="boolean", nullable=false) **/
  protected $admin = False;
  /** @Column(type="boolean", nullable=false) **/
  protected $activated = False;
  /**
   * @ManyToMany(targetEntity="Group")
   * @JoinTable(name="users_groups",
   *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
   *      inverseJoinColumns={@JoinColumn(name="group_id", referencedColumnName="id")}
   *      )
   */
  protected $groups;
  /** @OneToMany(targetEntity="User_Log", mappedBy="user",
        cascade={"persist","remove"})
  **  @JoinColumn(nullable=true, referencedColumnName="id") **/
  protected $log;

  public function __construct($username, $email) {
    $this->setUsername($username);
    $this->setEmail($email);
    $this->register_date = new \DateTime();
  }

  public function getID() {
    return $this->id;
  }

  public function setUsername($value) {
    $value = strtolower($value);
    if($value === alphanumeric($value,true)) {
      $this->username = $value;
    }
  }
  public function getUsername() {
    return $this->username;
  }

  public function setEmail($value) {
    $value = strtolower($value);
    if(filter_var($value, FILTER_VALIDATE_EMAIL)) {
      $this->email = $value;
      return true;
    }
    return false;
  }
  public function getEmail() {
    return $this->email;
  }

  public function setFirstname($value) {
    $this->firstname = $value;
  }
  public function getFirstname() {
    return $this->firstname;
  }

  public function setLastname($value) {
    $this->lastname = $value;
  }
  public function getLastname() {
    return $this->lastname;
  }

  public function setName($first,$last) {
    $this->setFirstname($first);
    $this->setLastname($last);
  }
  public function getName() {
    return $this->firstname." ".$this->lastname;
  }

  public function getGravatarUrl($size=300) {
    $hash = md5( strtolower( trim( $this->email ) ) );
    $url = "https://www.gravatar.com/avatar/".$hash."?size=".(int)$size."&d=".urlencode(URL_PATH."img/profile.jpg");
    return $url;
  }

  public function getProfilUrl() {
    return URL_PATH."user/".$this->username."/";
  }

  public function isActivated() {
    return $this->activated;
  }
  public function activate() {
    $this->activated = true;
  }
  public function deactivate() {
    $this->activated = false;
  }

  public function setPassword($pw) {
    $this->salt = Account::createSalt();
    $this->password = Account::createHash($pw,$this->getSalt());
  }
  public function getSalt() {
    return $this->salt;
  }
  public function checkPassword($pw) {
    return password_verify($pw,$this->password);
  }
  public function changePassword($old_pw, $new_pw, $admin=false) {
    if($admin instanceof entities\User) {
      if($admin->isAdmin()) {
        if($admin->checkPassword($old_pw)) {
          $this->setPassword($new_pw);
        }
      }
    }
    else if($this->checkPassword($old_pw)) {
      $this->setPassword($new_pw);
      return true;
    }
    return false;
  }

  public function makeAdmin() {
    $this->admin = True;
  }

  public function takeAdmin($value) {
    $this->admin = False;
  }
  public function isAdmin() {
    return $this->admin;
  }

  public function getRegisterDate() {
    return $this->register_date;
  }

  public function addGroup(Group $value) {
    $this->groups[] = $value;
  }
  public function getGroups() {
    return $this->groups;
  }

  public function addActivity(Group $value, $hash, $salt,$page) {
    $this->log[] = new User_Log($value, $this, $hash, $salt,$page);
    return true;
  }
  public function getActivity($hash=NULL) {
    if(empty($hash)) {
      return $this->$log;
    }
    else {
      $qb = Database::em()->qb();
      $qb->select("ua")->from("entities\User_Log","ua")->join("ua.user","u");
      $qb->where("ua.hash = :hash")->andWhere("u.id",$this)->setParameter("hash",$hash);
      $qb->orderBy("ua.id","DESC");
      return $qb->getQuery()->getResult();
    }
  }

  protected $sessionSalt;
  public function setSessionSalt($value) {
    $this->sessionSalt = $value;
  }
  public function getSessionSalt() {
    return $this->sessionSalt;
  }

  private $funcsalt = '{\PIB2)k}+FRQIm9%BLm:KhvavwRjRNuj*HIV75t&{"b.^h.!)d`n2n';
  public function getHash($reason = "username",$salt=NULL) {
    if($reason == "delete") {
      return @md5( $this->getID() . "deleteit" . $this->getEmail() );
    }
    if($reason == "register") {
      return @substr(md5(hash("haval160,4",$this->getUsername().$this->funcsalt."registerman")),-6);
    }
    else {
      return @hash("haval160,4",$salt.$this->getUsername());
    }
  }


}

 ?>
