<?php
namespace entities;
use entities\Group;
use Database;

/** @Entity
 ** @Table(name="email_log")
 ** @HasLifecycleCallbacks **/
class Email_Log {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $group;
  /** @ManyToOne(targetEntity="User")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $to;
  /** @Column(type="string", length=255, nullable=false) **/
  protected $betreff;
  /** @Column(type="boolean", nullable=false) **/
  protected $status = False;
  /** @Column(type="datetime") */
  protected $time;

  public function __construct(Group $group, User $user, $betreff, $status) {
    $this->group = $group;
    $this->to = $user;
    $this->betreff = $betreff;
    $this->status = $status;
    $this->time = new \DateTime();
  }

  public function getID() {
    return $this->id;
  }
  public function getTime() {
    return $this->time;
  }
  public function getUser() {
    return $this->to;
  }
  public function getSubject() {
    return $this->betreff;
  }


}

 ?>
