<?php
namespace entities;
use entities\Group;
use Database;

/** @Entity
 ** @Table(name="menu_item")
 ** @HasLifecycleCallbacks **/
class Menu_Item {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @Column(type="string", length=20, nullable=false) **/
  protected $title;
  /** @Column(type="string", length=40, nullable=true) **/
  protected $css;
  /** @Column(type="string", length=255, nullable=false) **/
  protected $path;
  /** @ManyToOne(targetEntity="Menu",inversedBy="items")
  **  @JoinColumn(nullable=false)*/
  protected $menu;

  public function __construct(Menu $menu, $title, $path, $css = NULL) {
    $this->menu = $menu;
    $this->setTitle($title);
    $this->setPath($path);
    $this->setCSS($css);
  }

  public function getID() {
    return $this->id;
  }

  public function setTitle($value) {
    $this->title = $value;
  }
  public function getTitle() {
    return $this->title;
  }

  public function setPath($value) {
    $this->path = $value;
  }
  public function getPath() {
    return $this->path;
  }

  public function setCSS($value) {
    $this->css = $value;
  }
  public function getCSS() {
    return $this->css;
  }


}

 ?>
