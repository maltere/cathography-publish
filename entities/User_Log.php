<?php
namespace entities;
use entities\Group;
use Database;

/** @Entity
 ** @Table(name="user_activity_log")
 ** @HasLifecycleCallbacks **/
class User_Log {
  /** @Id @GeneratedValue @Column(type="integer") **/
  protected $id;
  /** @ManyToOne(targetEntity="Group")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $group;
  /** @ManyToOne(targetEntity="User")
  **  @JoinColumn(nullable=false, referencedColumnName="id")*/
  protected $user;
  /** @Column(type="string", length=140, nullable=false) **/
  protected $hash;
  /** @Column(type="string", length=255, nullable=false) **/
  protected $salt;
  /** @Column(type="string", length=255, nullable=false) **/
  protected $page;
  /** @Column(type="datetime") */
  protected $time;

  public function __construct(Group $group, User $user, $hash, $salt, $page) {
    $this->group = $group;
    $this->user = $user;
    $this->hash = $hash;
    $this->salt = $salt;
    $this->setPage($page);
    $this->time = new \DateTime();
  }

  public function getID() {
    return $this->id;
  }
  public function isHash($hash) {
    return $this->hash == $hash;
  }
  public function getSalt() {
    return $this->salt;
  }
  public function getTime() {
    return $this->time;
  }
  public function getUser() {
    return $this->user;
  }
  public function getPage() {
    return $this->page;
  }
  protected function setPage($page) {
    return $this->page = $page;
  }

  public static function get($hash) {
    $qb = Database::em()->qb();
    $qb->select("ua")->from("entities\User_Log","ua");
    $qb->where("ua.hash = :hash")->setParameter("hash",$hash);
    $qb->orderBy("ua.id","DESC");
    return $qb->getQuery()->getResult();
  }


}

 ?>
